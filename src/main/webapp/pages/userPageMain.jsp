<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!Doctype html>
<html>
<head>
    <link href="${pageContext.servletContext.contextPath}/css/login.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.servletContext.contextPath}/css/seance.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.servletContext.contextPath}/css/header.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.servletContext.contextPath}/css/editMovie.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.servletContext.contextPath}/css/userPage.css" rel="stylesheet" type="text/css">
</head>
        <div class="content-userPage">
            <p class="message"><c:out value="${requestScope.error}"/></p>
            <form id="movieForm" name="movieForm" method="post" action="${pageContext.servletContext.contextPath}/editMovie?id=${movie.id}">
                <h3 class="formHeader">FirstName</h3>
                <h2 class="formHeader">${user.name}</h2><br />
                <h3 class="formHeader">Last name</h3>
                <h2 class="formHeader">${user.lastName}</h2><br />
                <h3 class="formHeader">Login</h3>
                <h2 class="formHeader">${user.login}</h2><br />
                <h3 class="formHeader">Email</h3>
                <h2 class="formHeader">${user.email}</h2><br />
            </form>

            <div class="tickets-info">
                <h3>TicketList</h3>
                <c:forEach items="${tickets}" var="ticket">
                    <p>${ticket}</p>
                </c:forEach>
            </div>
        </div>