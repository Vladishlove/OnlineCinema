<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tld/customTagLibrary" prefix="a" %>


<link href="${pageContext.servletContext.contextPath}/css/movies.css" rel="stylesheet" type="text/css">
<link href="${pageContext.servletContext.contextPath}/css/movie.css" rel="stylesheet" type="text/css">
<link href="${pageContext.servletContext.contextPath}/css/seance.css" rel="stylesheet" type="text/css">


<div class="filmInfo">
    <h3>Date: ${seance.dateTime.year} ${seance.dateTime.month} ${seance.dateTime.dayOfMonth}</h3>
    <h3>Time: ${seance.dateTime.hour}:${seance.dateTime.minute}</h3>
    <h3>Hall: ${seance.hall.name}</h3>
    <h3>Movie: ${seance.movie.movieName}</h3>
</div>
<div>
    <h3>You've bought ${ticketsAmountInOrder} ticket(s). Total price is ${ticketsAmountInOrder*seance.ticketPrice}.</h3>
    <a href=${pageContext.servletContext.contextPath}/>Main page</a>
</div>
