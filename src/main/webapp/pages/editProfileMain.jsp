<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!Doctype html>
<html>
<head>
    <link href="${pageContext.servletContext.contextPath}/css/login.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.servletContext.contextPath}/css/header.css" rel="stylesheet" type="text/css">
</head>
        <p class="message"><c:out value="${userRegistrationMessage}"/></p>
        <form name="loginForm" method="post" action="${pageContext.servletContext.contextPath}/editProfile">
            <h3 class="formHeader">First name</h3>
            <input class="inputField" type="text" name="firstName" value="${user.name}"/>

            <h3 class="formHeader">Last name</h3>
            <input class="inputField" type="text" name="lastName" value="${user.lastName}"/>

            <h3 class="formHeader">Login*</h3>
            <input class="inputField" type="text" name="login" value="${user.login}"/>

            <h3 class="formHeader">Email*</h3>
            <input class="inputField" type="text" name="email" value="${user.email}"/>

            <h3 class="formHeader">Password*</h3>
            <input class="inputField" type="password" name="password" value="${user.password}"/>

            <h3 class="formHeader">Submit password*</h3>
            <input class="inputField" type="password" value="${user.password}"/><br/>

            <input class="button" type="submit" value="Edit" />
        </form>
</body>
</html>