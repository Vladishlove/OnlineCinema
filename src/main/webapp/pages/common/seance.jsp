<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Seance</title>
    <link href="${pageContext.servletContext.contextPath}/css/login.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.servletContext.contextPath}/css/movies.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.servletContext.contextPath}/css/movie.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.servletContext.contextPath}/css/header.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.servletContext.contextPath}/css/seance.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="wrapper">
    <header class="header">
        <jsp:include page="headerUR.jsp" />
    </header>
    <main class="content">

        <div class="placeScheme">
            <jsp:include page="../seanceMain.jsp" />
        </div>
    </main>
</div>
<footer class="footer">

</footer>
</body>
</html>