<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!Doctype html>
<html>

<head>
    <link href="${pageContext.servletContext.contextPath}/css/login.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.servletContext.contextPath}/css/header.css" rel="stylesheet" type="text/css">
</head>
<div class="wrapper">
    <header class="header">
        <jsp:include page="headerUR.jsp" />
    </header>

    <main class="content">
        <div id="floater"></div>
        <form name="loginForm" method="post" action="${pageContext.servletContext.contextPath}/login">
            <h3 class="formHeader">Login</h3>
            <input class="inputField" type="text" name="login"/> <br/>
            <h3 class="formHeader">Password</h3>
            <input class="inputField" type="password" name="password"/> <br/>
            <input class="button" type="submit" value="Login" />
        </form>
        <a class="link" href=${pageContext.servletContext.contextPath}/register>Registration</a>
    </main>
</div>
<footer class="footer">

</footer>
</body>
</html>