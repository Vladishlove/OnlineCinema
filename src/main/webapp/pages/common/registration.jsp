<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!Doctype html>
<html>
<head>
    <link href="${pageContext.servletContext.contextPath}/css/login.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.servletContext.contextPath}/css/header.css" rel="stylesheet" type="text/css">
</head>
<div class="wrapper">
    <header class="header">
        <jsp:include page="headerUR.jsp" />
    </header>

    <main class="content">
        <p class="message"><c:out value="${userRegistrationMessage}"/></p>
        <form name="loginForm" method="post" action="${pageContext.servletContext.contextPath}/register">
            <h3 class="formHeader">First name</h3>
            <input class="inputField" type="text" name="firstName" value="${firstName}"/>

            <h3 class="formHeader">Last name</h3>
            <input class="inputField" type="text" name="LastName" value="${lastName}"/>

            <h3 class="formHeader">Login*</h3>
            <input class="inputField" type="text" name="login" value="${login}"/>

            <h3 class="formHeader">Email*</h3>
            <input class="inputField" type="text" name="email" value="${email}"/>

            <h3 class="formHeader">Password*</h3>
            <input class="inputField" type="password" name="password"/>

            <h3 class="formHeader">Submit password*</h3>
            <input class="inputField" type="password" name="passwordS"/><br/>

            <input class="button" type="submit" value="Register" />
        </form>
    </main>
</div>

</body>
</html>