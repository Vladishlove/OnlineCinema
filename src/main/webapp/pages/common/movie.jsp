<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<head>
    <title>MovieList</title>
    <link href="${pageContext.servletContext.contextPath}/css/login.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.servletContext.contextPath}/css/movies.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.servletContext.contextPath}/css/header.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.servletContext.contextPath}/css/movie.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="wrapper">
    <header class="header">
        <jsp:include page="headerUR.jsp"/>
    </header>

    <main class="content">
        <div class="mainContent">
            <jsp:include page="../movieMain.jsp"/>
        </div>
    </main>
</div>
</body>
</html>