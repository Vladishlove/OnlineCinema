<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tld/customTagLibrary" prefix="a" %>


<link href="${pageContext.servletContext.contextPath}/css/movies.css" rel="stylesheet" type="text/css">
<link href="${pageContext.servletContext.contextPath}/css/movie.css" rel="stylesheet" type="text/css">
<link href="${pageContext.servletContext.contextPath}/css/seance.css" rel="stylesheet" type="text/css">


    <div class="filmInfo">
        <h3>Date: ${seance.dateTime.year} ${seance.dateTime.month} ${seance.dateTime.dayOfMonth}</h3>
        <h3>Time: ${seance.dateTime.hour}:${seance.dateTime.minute}</h3>
        <h3>Hall: ${seance.hall.name}</h3>
        <h3>Movie: ${seance.movie.movieName}</h3>
        <h3>Ticket price: ${seance.ticketPrice}</h3>
    </div>
    <div>
        <form method="post" action="${pageContext.servletContext.contextPath}/order">
            <p><b>Hall scheme</b></p>
            <c:forEach var="r" begin="1" end="${seance.hall.rowQuantity}">
                <c:forEach var="p" begin="1" end="${seance.hall.placeQuantity}">
                    <c:choose>
                        <c:when test="${seance.placesMap[r-1][p-1] == true}">
                            <input type="checkbox" name="rowPlace" value= ${r}:${p} disabled>
                        </c:when>
                        <c:otherwise>
                            <input type="checkbox" name="rowPlace" value= ${r}:${p}>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
                <br />
            </c:forEach>
            <c:set var="seanceId" value="${seance.id}" scope="session"/>
            <p><input class="button" type="submit" value="Buy"></p>
        </form>
    </div>