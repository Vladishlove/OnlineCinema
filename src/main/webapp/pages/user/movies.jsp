<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>MovieList</title>
    <link href="${pageContext.servletContext.contextPath}/css/login.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.servletContext.contextPath}/css/movies.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.servletContext.contextPath}/css/header.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="wrapper">
    <header class="header">
        <jsp:include page="headerU.jsp" />

    </header>

    <main class="content">

        <div id="floater">
            <h2>Placard</h2>
        </div>
        <div class="dates-holder">

            <c:forEach items="${dates}" var="date">
                <c:choose>
                    <c:when test="${date.dayOfMonth == (selected.dayOfMonth)}">
                        <a href="${pageContext.servletContext.contextPath}/movieList?selected=${date}"
                           class="date-block data-item
            pressed">
                            <span class="date">${date.dayOfMonth}</span>
                            <span class="date">${date.month}</span>
                            <span class="date">${date.dayOfWeek}</span>
                        </a>
                    </c:when>
                    <c:otherwise>
                        <a href="${pageContext.servletContext.contextPath}/movieList?selected=${date}"
                           class="date-block data-item">
                            <span class="date">${date.dayOfMonth}</span>
                            <span class="date">${date.month}</span>
                            <span class="date">${date.dayOfWeek}</span>
                        </a>
                    </c:otherwise>
                </c:choose>
            </c:forEach>

        </div>

        <div class="film-box-holder actual">
            <c:forEach items="${movies}" var="movies">
                <%--<a href="${pageContwext.servletContext.contextPath}/movie?id=${movies.id}"></a>--%>

                <div class="film-box ">
                    <div class="img-holder">
                        <a href="${pageContext.servletContext.contextPath}/movie?id=${movies.id}">
                            <img src="${pageContext.servletContext.contextPath}/images/moviename.jpg" alt="${movies.movieName}">
                        </a>
                    </div>
                    <div class="sub-info">
                        <a href="${pageContext.servletContext.contextPath}/movie?id=${movies.id}"
                           class="film-title"><span>"${movies.movieName}"</span></a>
                        </a>
                    </div>
                </div>
            </c:forEach>
        </div>
    </main>
</div>
<footer class="footer">

</footer>
</body>
</html>