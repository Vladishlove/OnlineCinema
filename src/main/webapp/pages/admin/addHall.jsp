<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!Doctype html>
<html>
<head>
    <meta charset="utf-8">
    <link href="${pageContext.servletContext.contextPath}/css/login.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.servletContext.contextPath}/css/header.css" rel="stylesheet" type="text/css">
</head>
<div class="wrapper">
    <header class="header">
        <jsp:include page="headerA.jsp" />
    </header>

    <main class="content">
        <p class="message"><c:out value="${hallMessage}"/></p>
        <form name="loginForm" method="post" action="${pageContext.servletContext.contextPath}/addHall">
            <h3 class="formHeader">Hall name</h3>
            <input class="inputField" type="text" name="hallName"/>

            <h3 class="formHeader">Row quantity</h3>
            <input class="inputField" type="number" name="rowQuantity"/>

            <h3 class="formHeader">Place quantity</h3>
            <input class="inputField" type="number" name="placeQuantity"/><br />

            <input class="button" type="submit" value="Save" />
        </form>
    </main>
</div>

</body>
</html>