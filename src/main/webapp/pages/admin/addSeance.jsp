<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!Doctype html>
<html>
<head>
    <title>AddMovie</title>
    <meta charset="utf-8">
    <link href="${pageContext.servletContext.contextPath}/css/login.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.servletContext.contextPath}/css/seance.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.servletContext.contextPath}/css/header.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.servletContext.contextPath}/css/editMovie.css" rel="stylesheet" type="text/css">

</head>
<div class="wrapper">
    <header class="header">
        <jsp:include page="headerA.jsp"/>
    </header>

    <main class="content">
        <div class="content-description">
            <p class="message"><c:out value="${requestScope.error}"/></p>
            <form name="addSeance" method="post" action="${pageContext.servletContext.contextPath}/addSeance">
                <p>${seanceMessage}</p>
                <h3 class="formHeader">Set date and time of seance:</h3>
                <input type="datetime-local" id="myLocalDate" name="dateTime" value="2016-10-15T15:25:01"><br/>

                <h3 class="formHeader">Set the movie:</h3>
                <select class="inputField" name="movies">
                <c:forEach items="${movies}" var="movie">
                    <option value="${movie.id}">${movie.movieName}</option>
                </c:forEach>
                </select> <br/>

                <h3 class="formHeader">Set the hall:</h3>
                <select class="inputField" name="halls">
                <c:forEach items="${halls}" var="hall">
                    <option value="${hall.id}">${hall.name}</option>
                </c:forEach>
                </select> <br/>

                <h3 class="formHeader">Ticket price:</h3>
                <input class="inputField" type="number" name="ticketPrice"/> <br/>

                <input class="button" type="submit" value="Add seance"/> <br />
                <a href=${pageContext.servletContext.contextPath}/movieList>Main page</a>

            </form>

        </div>
    </main>
</div>

</body>
</html>