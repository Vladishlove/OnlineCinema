<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!Doctype html>
<html>
<head>
    <title>AddMovie</title>
    <link href="${pageContext.servletContext.contextPath}/css/login.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.servletContext.contextPath}/css/seance.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.servletContext.contextPath}/css/header.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.servletContext.contextPath}/css/editMovie.css" rel="stylesheet" type="text/css">

</head>
<div class="wrapper">
    <header class="header">
        <meta charset="utf-8">
        <jsp:include page="headerA.jsp"/>
    </header>

    <main class="content">
        <div class="adminControl">
            <a href="${pageContext.servletContext.contextPath}/deleteMovie?id=${seance.id}">Delete seance</a>
        </div>
        <div class="content-description">
            <form name="editSeance" method="post" action="${pageContext.servletContext.contextPath}/editSeance?id=${seance.id}">
                <p>${editSeanceMessage}</p>
                <h3 class="formHeader">Set date and time of seance:</h3>
                <input type="datetime-local" id="myLocalDate" name="dateTime" value="${seance.dateTime}"><br/>

                <h3 class="formHeader">Set the movie:</h3>
                <select class="inputField" name="movies">
                    <c:forEach items="${movies}" var="movie">
                        <c:if test="${movie.id == seance.movie.id}">
                            <option value="${movie.id}" selected>${movie.movieName}</option>
                        </c:if>
                        <option value="${movie.id}">${movie.movieName}</option>
                    </c:forEach>
                </select> <br/>

                <h3 class="formHeader">Set the hall:</h3>
                <select class="inputField" name="halls">
                    <c:forEach items="${halls}" var="hall">
                        <c:if test="${hall.id == seance.hall.id}">
                            <option value="${hall.id}" selected>${hall.name}</option>
                        </c:if>
                        <option value="${hall.id}">${hall.name}</option>
                    </c:forEach>
                </select> <br/>

                <h3 class="formHeader">Ticket price:</h3>
                <input class="inputField" type="number" name="ticketPrice" value="${seance.ticketPrice}"/> <br/>

                <input class="button" type="submit" value="Edit seance"/> <br />
                <a href=${pageContext.servletContext.contextPath}/movieList>Main page</a>

            </form>

        </div>
    </main>
</div>

</body>
</html>