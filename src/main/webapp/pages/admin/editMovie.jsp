<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!Doctype html>
<html>
<head>
    <title>Edit movie</title>
    <meta charset="utf-8">
    <link href="${pageContext.servletContext.contextPath}/css/login.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.servletContext.contextPath}/css/seance.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.servletContext.contextPath}/css/header.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.servletContext.contextPath}/css/editMovie.css" rel="stylesheet" type="text/css">

</head>
<div class="wrapper">
    <header class="header">
        <jsp:include page="headerA.jsp"/>
    </header>

    <main class="content">
        <div class="adminControl">
            <a href="${pageContext.servletContext.contextPath}/deleteMovie?id=${movie.id}">Delete movie</a>
        </div>

        <div class="content-description">
            <p class="message"><c:out value="${requestScope.error}"/></p>
            <form id="movieForm" name="movieForm" method="post" action="${pageContext.servletContext.contextPath}/editMovie?id=${movie.id}">
                <h3 class="formHeader">Title</h3>
                <input type="text" id="movieName" name="movieName" value="${movie.movieName}">

                <h3 class="formHeader">In Theaters</h3>
                <input type="date" id="myDate" name="openingDate" value="${movie.openingDate}">

                <h3 class="formHeader">Country</h3>
                <input class="inputField" type="text" name="country" value="${movie.country}"/>

                <h3 class="formHeader">Starring</h3>
                <input class="inputField" type="text" name="cast" value="${movie.cast}"/>

                <h3 class="formHeader">Director</h3>
                <input class="inputField" type="text" name="director" value="${movie.director}"/>

                <h3 class="formHeader">Writer(s)</h3>
                <input class="inputField" type="text" name="writer" value="${movie.writer}"/>

                <h3 class="formHeader">Run Time</h3>
                <input class="inputField" type="text" name="duration" value="${movie.duration}"/>

                <h3 class="formHeader">Start date</h3>
                <input class="inputField" type="date" name="startDate" value="${movie.startDate.toLocalDate()}"/>

                <h3 class="formHeader">End date</h3>
                <input class="inputField" type="date" name="endDate" value="${movie.endDate.toLocalDate()}"/><br/>

                <input class="button" type="submit" value="Submit"/>
            </form>

            <div class="description-info">
                <h3>Description</h3>
                <textarea rows="10" cols="50" name="description" form="movieForm">
                ${movie.description}
                </textarea>
            </div>
        </div>
    </main>
</div>

</body>
</html>