<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tld/customTagLibrary" prefix="a" %>


<link href="${pageContext.servletContext.contextPath}/css/movies.css" rel="stylesheet" type="text/css">
<link href="${pageContext.servletContext.contextPath}/css/movie.css" rel="stylesheet" type="text/css">
<link href="${pageContext.servletContext.contextPath}/css/seance.css" rel="stylesheet" type="text/css">


<div class="content-description-wrap">
    <div class="movieTitle">
        <h2>${movie.movieName}</h2>
    </div>
    <div class="tab content-description" style="visibility: visible; display: block;">
        <div class="aside-info">
            <dl class="film-data-list">
                <%--Премьера--%>
                <dt>In Theaters:</dt>
                <dd>test</dd>

                <dt>Country:</dt>
                <dd>test</dd>

                <dt>Starring:</dt>
                <dd>test</dd>

                <dt>Director(s):</dt>
                <dd>test</dd>

                <dt>Writer(s):</dt>
                <dd>test</dd>

                <dt>Run Time:</dt>
                <dd>${movie.duration}</dd>

            </dl>
        </div>
        <div class="description-info">
            <div class="text-block">
                <span>Description:</span><br />
                ${movie.description}
            </div>
        </div>
    </div>
</div>
<div class="dates-holder">

    <c:forEach items="${dates}" var="date">
        <c:choose>
            <c:when test="${date.dayOfMonth == selected.dayOfMonth}">
                <a href="${pageContext.servletContext.contextPath}/movie?selected=${date}&id=${movie.id}"
                   class="date-block data-item
            pressed">
                    <span class="date">${date.dayOfMonth}</span>
                    <span class="date">${date.month}</span>
                    <span class="date">${date.dayOfWeek}</span>
                </a>
            </c:when>
            <c:otherwise>
                <a href="${pageContext.servletContext.contextPath}/movie?selected=${date}&id=${movie.id}"
                   class="date-block data-item">
                    <span class="date">${date.dayOfMonth}</span>
                    <span class="date">${date.month}</span>
                    <span class="date">${date.dayOfWeek}</span>
                </a>
            </c:otherwise>
        </c:choose>
    </c:forEach>
</div>


<div class="schedule-section-holder">

    <div class="schedule-box">
        <div class="scheduleTitle">
            <h3>Schedule</h3>
        </div>

        <div class="schedule-row ">

            <div data-sort="day" class="schedule-holder">

                <div class="schedule-frame">

                    <ul class="schedule-list">
                        <c:forEach items="${seances}" var="seances">

                            <c:if test="${seances.dateTime.dayOfMonth==selected.dayOfMonth}">
                                <li>
                                    <a href="${pageContext.servletContext.contextPath}/seance?id=${seances.id}">
                                        <span class="price">${seances.dateTime.hour}:${seances.dateTime.minute}</span>
                                            <span>${seances.hall.name}</span>
                                    </a>
                                </li>
                            </c:if>
                        </c:forEach>
                    </ul>
                </div>

            </div>
        </div>

    </div>
</div>