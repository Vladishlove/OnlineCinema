package ua.org.oa.neplokhv.cinema.service.impl;

import ua.org.oa.neplokhv.cinema.dao.DaoFactory;
import ua.org.oa.neplokhv.cinema.dao.api.Dao;
import ua.org.oa.neplokhv.cinema.dto.HallDto;
import ua.org.oa.neplokhv.cinema.mapper.BeanMapper;
import ua.org.oa.neplokhv.cinema.model.Hall;
import ua.org.oa.neplokhv.cinema.service.api.Service;

import java.util.List;

/**
 * Created by student on 13.09.2016.
 */
public class HallServiceImpl implements Service<Integer, HallDto> {

    private static HallServiceImpl service;
    private Dao<Integer, Hall> hallDao;
    private BeanMapper beanMapper;

    private HallServiceImpl() {
        hallDao = DaoFactory.getInstance().getHallDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized HallServiceImpl getInstance() {
        if (service == null) {
            service = new HallServiceImpl();
        }
        return service;
    }

    @Override
    public List<HallDto> getAll() {
        List<Hall> halls = hallDao.getAll();
        List<HallDto> hallDtos = beanMapper.mapAll(halls, HallDto.class);
        return hallDtos;
    }

    @Override
    public List<HallDto> getAllOrderedBy(String colName) {
        return null;
    }

    @Override
    public List<HallDto> getByFKey(Integer key, String entityName) {
        return null;
    }

    @Override
    public List<HallDto> getByStringColumnValue(String columnName, String value) {
        return null;
    }

    @Override
    public HallDto getById(Integer key) {
        HallDto hallDto = beanMapper.singleMapper(hallDao.getById(key), HallDto.class);
        return hallDto;
    }

    @Override
    public void save(HallDto elem) {
        Hall hall = beanMapper.singleMapper(elem, Hall.class);
        hallDao.save(hall);
    }

    @Override
    public void delete(Integer key) {
        hallDao.delete(key);
    }

    @Override
    public void update(HallDto elem) {
        Hall hall = beanMapper.singleMapper(elem, Hall.class);
        hallDao.update(hall);
    }
}