package ua.org.oa.neplokhv.cinema.controllers;

import ua.org.oa.neplokhv.cinema.dto.UserDto;
import ua.org.oa.neplokhv.cinema.web.Manager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static ua.org.oa.neplokhv.cinema.helpers.FileHelper.pathSeparator;

/**
 * Created by Vlad on 10/9/2016.
 */
public class EditProfileServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDto userDto = (UserDto) request.getSession().getAttribute("user");
        try {
            String login = request.getParameter("login");
            String email = request.getParameter("email");
            if (!userDto.getLogin().equals(login) || !userDto.getEmail().equals(email)) {
                Manager.isUnicValue(userDto, login, email);
            }
            userDto.setLogin(login);
            userDto.setEmail(email);
            userDto.setName(request.getParameter("firstName"));
            userDto.setLastName(request.getParameter("lastName"));


            Manager.updateUser(userDto);
            response.sendRedirect(getServletContext().getContextPath()+pathSeparator+

                    "pages"+pathSeparator+userDto.getRole().getName()+pathSeparator+"userPage.jsp");
        } catch (IllegalArgumentException e) {
            request.getSession().setAttribute("editProfileMessage", e.getMessage());
            response.sendRedirect(getServletContext().getContextPath()+pathSeparator+
                    "pages"+pathSeparator+userDto.getRole().getName()+pathSeparator+"editProfile.jsp");
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDto userDto = (UserDto) request.getSession().getAttribute("user");
            response.sendRedirect(getServletContext().getContextPath()+pathSeparator+
                    "pages"+pathSeparator+userDto.getRole().getName()+pathSeparator+"editProfile.jsp");
    }
}