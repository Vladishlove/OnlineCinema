package ua.org.oa.neplokhv.cinema.controllers;

import ua.org.oa.neplokhv.cinema.dto.MovieDto;
import ua.org.oa.neplokhv.cinema.dto.UserDto;
import ua.org.oa.neplokhv.cinema.helpers.DateNavigation;
import ua.org.oa.neplokhv.cinema.model.Movie;
import ua.org.oa.neplokhv.cinema.service.impl.MovieServiceImpl;
import ua.org.oa.neplokhv.cinema.service.impl.UserServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import static ua.org.oa.neplokhv.cinema.helpers.FileHelper.pathSeparator;


/**
 * Created by Vlad on 9/26/2016.
 */
public class MovieListServlet extends HttpServlet{

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LocalDateTime selectedDate;
        LocalDateTime sessionAttr = (LocalDateTime) request.getSession().getAttribute("selected");
        String requestParameter = request.getParameter("selected");
        selectedDate = DateNavigation.setSelectedDate(sessionAttr, requestParameter);
        request.getSession().setAttribute("selected", selectedDate);

        MovieServiceImpl movieService = MovieServiceImpl.getInstance();
        List<MovieDto> movieList = movieService.getAll();
        List<MovieDto> movieListInDate = new ArrayList<>();
        List<LocalDateTime> dateList = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            dateList.add(LocalDateTime.now().plusDays(i));
        }
        for (MovieDto movieDto : movieList) {
            if (movieDto.getStartDate().isAfter(selectedDate) ||
                    movieDto.getEndDate().plusDays(1).isBefore(selectedDate)) {
                System.out.println("inmovielist " + "strt: "+movieDto.getStartDate()+" end: " + movieDto.getEndDate() +
                " selected: " + selectedDate);
            }
            else {
                movieListInDate.add(movieDto);
            }
        }
        request.getSession().setAttribute("movies", movieListInDate);
        request.getSession().setAttribute("dates", dateList);
        if (request.getSession().getAttribute("user") != null) {
            UserDto user = (UserDto) request.getSession().getAttribute("user");
            if (user.getRole().getName().equals("user")) {
                response.sendRedirect(getServletContext().getContextPath()+pathSeparator+"pages"+pathSeparator+"user"+pathSeparator+"movies.jsp");
            }
            else if (user.getRole().getName().equals("admin")) {
                response.sendRedirect(getServletContext().getContextPath()+pathSeparator+"pages"+pathSeparator+"admin"+pathSeparator+"movies.jsp");
            }
        }
        else {
            response.sendRedirect(getServletContext().getContextPath()+pathSeparator+"pages"+pathSeparator+"common"+pathSeparator+"movies.jsp");
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}