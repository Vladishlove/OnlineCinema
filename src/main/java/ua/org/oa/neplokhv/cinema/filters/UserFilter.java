package ua.org.oa.neplokhv.cinema.filters;

/**
 * Created by Vlad on 9/28/2016.
 */

import ua.org.oa.neplokhv.cinema.dto.UserDto;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by dmitr on 13.06.2016.
 */

public class UserFilter implements Filter {
    public void destroy() {
    }
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        UserDto userDTO = (UserDto) request.getSession().getAttribute("user");
        if (userDTO != null && (userDTO.getRole().getName().equals("user") ||
                userDTO.getRole().getName().equals("admin"))) {
            chain.doFilter(req, resp);
        }
        else {
            response.sendRedirect(request.getContextPath() + "/pages/errors/roleUserErrorPage.jsp");
        }
    }
    public void init(FilterConfig config) throws ServletException {
    }
}