package ua.org.oa.neplokhv.cinema.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vlad on 12.08.16.
 */
public class Storage<V extends Entity> implements GenericStorage<Long, V> {

    private long id = 0;

    List<Node<Long, V>> nodeList = new ArrayList<>();

    @Override
    public long create(V value) {
        nodeList.add(new Node(++id, value));
        return id;
    }

    @Override
    public V read(long id) {
        V value = null;
        for (Node<Long,V> o : nodeList) {
            if (o.getKey().equals(id)) return o.getValue();
        }
        return value;
    }

    @Override
    public boolean update(long id, V obj) {
        return false;
    }

    @Override
    public boolean delete(long id) {
        return false;
    }

    @Override
    public List<V> readAll() {
        return null;
    }

    @Override
    public int size() {
        return 0;
    }
}