package ua.org.oa.neplokhv.cinema.controllers;


import ua.org.oa.neplokhv.cinema.dto.RoleDto;
import ua.org.oa.neplokhv.cinema.dto.UserDto;
import ua.org.oa.neplokhv.cinema.service.impl.HallServiceImpl;
import ua.org.oa.neplokhv.cinema.service.impl.MovieServiceImpl;
import ua.org.oa.neplokhv.cinema.service.impl.UserServiceImpl;
import ua.org.oa.neplokhv.cinema.web.Manager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

import static ua.org.oa.neplokhv.cinema.helpers.FileHelper.pathSeparator;

/**
 * Created by Vlad on 9/27/2016.
 */
public class AddHallServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            Manager.createHall(
                    request.getParameter("hallName"),
                    Integer.parseInt(request.getParameter("rowQuantity")),
                    Integer.parseInt(request.getParameter("placeQuantity")));
            PrintWriter writer = response.getWriter();
            writer.write("Hall was created");
            Thread.currentThread().sleep(1000);
            response.sendRedirect(getServletContext().getContextPath() +
                    pathSeparator + "pages" + pathSeparator
                    + "admin" + pathSeparator + "movies.jsp");
        } catch (IllegalArgumentException e) {
            request.getSession().setAttribute("hallMessage", e.getMessage());
            response.sendRedirect(getServletContext().getContextPath() +
                    pathSeparator + "pages" + pathSeparator
                    + "admin" + pathSeparator + "addHall.jsp");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect(getServletContext().getContextPath()+pathSeparator+
                "pages"+pathSeparator+"admin"+pathSeparator+"addHall.jsp");
        }
}
