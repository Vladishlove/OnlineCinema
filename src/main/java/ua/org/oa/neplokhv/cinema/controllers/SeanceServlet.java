package ua.org.oa.neplokhv.cinema.controllers;

import ua.org.oa.neplokhv.cinema.dto.MovieDto;
import ua.org.oa.neplokhv.cinema.dto.SeanceDto;
import ua.org.oa.neplokhv.cinema.dto.UserDto;
import ua.org.oa.neplokhv.cinema.model.User;
import ua.org.oa.neplokhv.cinema.service.impl.MovieServiceImpl;
import ua.org.oa.neplokhv.cinema.service.impl.SeanceServiceImpl;
import ua.org.oa.neplokhv.cinema.web.Manager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import static ua.org.oa.neplokhv.cinema.helpers.FileHelper.pathSeparator;

/**
 * Created by Vlad on 9/27/2016.
 */
public class SeanceServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String idStr = request.getParameter("id");
        UserDto userDto = (UserDto) request.getSession().getAttribute("user");

        try {
            int id = Integer.parseInt(idStr);
            SeanceDto seanceDto = SeanceServiceImpl.getInstance().getById(id);
            request.setAttribute("seance", seanceDto);
            request.setAttribute("seanceTime", seanceDto.getDateTime());
            request.setAttribute("hall", seanceDto.getHall());
            request.setAttribute("soldTickets", seanceDto.getSoldTickets());
            if (userDto != null) {
                //System.out.println(userDto);
                request.getRequestDispatcher(
                        "pages" + pathSeparator +
                                userDto.getRole().getName() + pathSeparator +"seance.jsp").forward(request,response);
            }
            else {
                request.getRequestDispatcher("pages" + pathSeparator +
                        "common"+pathSeparator+"seance.jsp").forward(request,response);
            }
        } catch (NumberFormatException | NoSuchElementException e) {
            e.printStackTrace();
            response.sendRedirect("pages" + pathSeparator +
                    "common"+pathSeparator+"sorry.jsp");
        }

        }
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }
}