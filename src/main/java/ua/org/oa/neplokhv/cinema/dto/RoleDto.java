package ua.org.oa.neplokhv.cinema.dto;

import ua.org.oa.neplokhv.cinema.model.Entity;

/**
 * Created by Vlad on 9/18/2016.
 */
public class RoleDto extends Entity{


    private String name;
    private static final String admin = "admin";
    private static final String user = "user";

    public RoleDto(String name) {
        setName(name);
    }
    public RoleDto() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.equals(admin) || name.equals(user)){
            this.name = name;
        }
        else throw new IllegalArgumentException("not supported role name");
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + getId() +
                ", name='" + name + '\'' +
                '}';
    }
}