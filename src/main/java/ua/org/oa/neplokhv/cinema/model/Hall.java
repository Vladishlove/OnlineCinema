package ua.org.oa.neplokhv.cinema.model;

import java.io.Serializable;

/**
 * Created by vlad on 12.08.16.
 */
public class Hall extends Entity implements Serializable{
    private int rowQuantity;
    private int placeQuantity;
    private String name;
    public Hall(String name, int pq, int rq) {
        setName(name);
        setPlaceQuantity(pq);
        setRowQuantity(rq);
    }
    public Hall() {

    }

    public int getRowQuantity() {
        return rowQuantity;
    }

    public void setRowQuantity(int rowQuantity) {
        this.rowQuantity = rowQuantity;
    }

    public int getPlaceQuantity() {
        return placeQuantity;
    }

    public void setPlaceQuantity(int placeQuantity) {
        this.placeQuantity = placeQuantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Hall{" +
                "name='" + name + '\'' +
                '}';
    }
}
