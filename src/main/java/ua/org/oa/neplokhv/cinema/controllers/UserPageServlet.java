package ua.org.oa.neplokhv.cinema.controllers;

import ua.org.oa.neplokhv.cinema.dto.TicketDto;
import ua.org.oa.neplokhv.cinema.dto.UserDto;
import ua.org.oa.neplokhv.cinema.web.Manager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static ua.org.oa.neplokhv.cinema.helpers.FileHelper.pathSeparator;

/**
 * Created by Vlad on 10/9/2016.
 */
public class UserPageServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDto userDto = (UserDto) request.getSession().getAttribute("user");
        List<String> ticketsView = new ArrayList<>();
        for (TicketDto ticketDto : userDto.getTicketList()) {
            ticketsView.add(Manager.viewTicket(ticketDto));
        }
        request.getSession().setAttribute("tickets", ticketsView);
        response.sendRedirect(getServletContext().getContextPath()+pathSeparator+"pages"+pathSeparator+userDto.getRole().getName()
                +pathSeparator+"userPage.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}