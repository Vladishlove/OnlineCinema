package ua.org.oa.neplokhv.cinema.model;

import java.util.List;

/**
 * Created by vlad on 12.08.16.
 */
public interface GenericStorage<K, V extends Entity> {

    class Node<K, V>{
        K key;
        V value;
        public Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public K getKey() {
            return key;
        }

        public void setKey(K key) {
            this.key = key;
        }

        public V getValue() {
            return value;
        }

        public void setValue(V value) {
            this.value = value;
        }
    }

    long create(V obj);
    V read(long id);
    boolean update(long id, V obj);
    boolean delete(long id);
    List<V> readAll();
    int size();
}