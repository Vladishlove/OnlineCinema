package ua.org.oa.neplokhv.cinema.controllers;


import ua.org.oa.neplokhv.cinema.dto.HallDto;
import ua.org.oa.neplokhv.cinema.dto.MovieDto;
import ua.org.oa.neplokhv.cinema.dto.SeanceDto;
import ua.org.oa.neplokhv.cinema.model.Seance;
import ua.org.oa.neplokhv.cinema.service.impl.HallServiceImpl;
import ua.org.oa.neplokhv.cinema.service.impl.MovieServiceImpl;
import ua.org.oa.neplokhv.cinema.service.impl.SeanceServiceImpl;
import ua.org.oa.neplokhv.cinema.web.Manager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import static ua.org.oa.neplokhv.cinema.helpers.FileHelper.pathSeparator;

/**
 * Created by Vlad on 9/27/2016.
 */
public class EditSeanceServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String htm =
                "<body>" +
                        "<p>" + "Seance is edited" +
                        "<p>" + "Time: " + request.getParameter("dateTime").replace("T", " ") + "<br/>" +
                        "Hall id: " + request.getParameter("halls") + "<br/>" +
                        "Movie id: " + request.getParameter("movies") + "<br/>" +
                        "ticketPrice: " + request.getParameter("ticketPrice") + "<br />";

        try {
            SeanceDto seanceDto = SeanceServiceImpl.getInstance().getById(Integer.parseInt(request.getParameter("id")));
            if (seanceDto == null) throw new IllegalArgumentException("no such session id");
            Manager.updateSeance(
                    seanceDto,
                    MovieServiceImpl.getInstance().getById(Integer.parseInt(request.getParameter("movies"))),
                    HallServiceImpl.getInstance().getById(Integer.parseInt(request.getParameter("halls"))),
                    LocalDateTime.parse(request.getParameter("dateTime")),
                    Integer.parseInt(request.getParameter("ticketPrice")));
            response.sendRedirect(getServletContext().getContextPath() + pathSeparator + "seance?id="+seanceDto.getId());
        } catch (IllegalArgumentException e) {
            request.getSession().setAttribute("editSeanceMessage", e.getMessage());
            response.sendRedirect(getServletContext().getContextPath() +
                    pathSeparator + "pages" + pathSeparator
                    + "admin" + pathSeparator + "editSeance.jsp");
        }
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<MovieDto> movieDtos = MovieServiceImpl.getInstance().getAll();
        List<HallDto> hallDtos = HallServiceImpl.getInstance().getAll();
        getServletContext().setAttribute("movies", movieDtos);
        getServletContext().setAttribute("halls", hallDtos);
        request.getSession().setAttribute("seance", SeanceServiceImpl.
                getInstance().getById(Integer.parseInt(request.getParameter("id"))));
        response.sendRedirect(getServletContext().getContextPath() + pathSeparator +
                "pages" + pathSeparator +
                "admin" + pathSeparator + "editSeance.jsp");
    }
}