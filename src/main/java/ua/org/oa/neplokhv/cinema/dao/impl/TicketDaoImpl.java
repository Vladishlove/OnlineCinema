package ua.org.oa.neplokhv.cinema.dao.impl;

import ua.org.oa.neplokhv.cinema.dao.DaoFactory;
import ua.org.oa.neplokhv.cinema.model.Ticket;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tssupport15 on 13.09.16.
 */
public class TicketDaoImpl extends CrudDao<Ticket> {

    private final String INSERT = "INSERT INTO " +
            "cinema.ticket(row, place, user_idUser, seance_idseance, isSold) " +
            "VALUES (?,?,?,?,?)";
    private final String UPDATE = "UPDATE cinema.ticket " +
            "SET row = ?, place = ?, user_idUser = ?, seance_idseance = ?, isSold = ? " +
            "WHERE idticket = ?";


    UserDaoImpl userDao;
    SeanceDaoImpl seanceDao;
    private static TicketDaoImpl crudDao;
    public TicketDaoImpl(Class type) {
        super(type);
    }

    public static synchronized TicketDaoImpl getInstance() {
        if (crudDao == null) {
            crudDao = new TicketDaoImpl(Ticket.class);

        }
        return crudDao;
    }

    @Override
    public PreparedStatement createInsertPreparedStatement(Connection connection, Ticket elem) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setInt(1, elem.getRow());
        preparedStatement.setInt(2, elem.getPlace());
        preparedStatement.setInt(3, elem.getUserId());
        preparedStatement.setInt(4, elem.getSeanceId());
        preparedStatement.setBoolean(5, elem.isSold());
        return preparedStatement;
    }
    @Override
    public PreparedStatement createUpdatePreparedStatement(Connection connection, Ticket elem) throws SQLException{
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        preparedStatement.setInt(1, elem.getRow());
        preparedStatement.setInt(2, elem.getPlace());
        preparedStatement.setInt(3, elem.getUserId());
        preparedStatement.setInt(4, elem.getSeanceId());
        preparedStatement.setBoolean(5, elem.isSold());
        preparedStatement.setInt(6, elem.getId());
        return preparedStatement;
    }
    @Override
    public List<Ticket> readAll(ResultSet rs) throws SQLException {
        List<Ticket> tickets = new ArrayList<>();
        seanceDao = (SeanceDaoImpl) DaoFactory.getInstance().getSeanceDao();
        Ticket ticket;
        while (rs.next()) {
            ticket = new Ticket();
            ticket.setId(rs.getInt("idticket"));
            ticket.setPlace(rs.getInt("place"));
            ticket.setRow(rs.getInt("row"));
            ticket.setUserId(rs.getInt("user_idUser"));
            ticket.setSeanceId(rs.getInt("seance_idseance"));
            tickets.add(ticket);
        }
        return tickets;
    }
}