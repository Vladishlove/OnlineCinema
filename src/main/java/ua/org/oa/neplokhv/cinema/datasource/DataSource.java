package ua.org.oa.neplokhv.cinema.datasource;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import ua.org.oa.neplokhv.cinema.helpers.PropertyHolder;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by Vlad on 9/12/2016.
 */
public final class DataSource {
    private static ComboPooledDataSource pooledDataSource;
    private static DataSource dataSource;

    private DataSource() {
        initPoolConnections();
    }

    public static synchronized DataSource getInstance(){
        if (dataSource == null) {
            dataSource = new DataSource();
        }
        return dataSource;
    }

    public Connection getConnection() {
        Connection connection = null;
        try {
            connection = pooledDataSource.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    private void initPoolConnections() {
        pooledDataSource = new ComboPooledDataSource();
        PropertyHolder propHolder = PropertyHolder.getInstance();
        try {
            pooledDataSource.setDriverClass(propHolder.getDbDriver());
            pooledDataSource.setJdbcUrl(propHolder.getURL());
            pooledDataSource.setUser(propHolder.getUserLogin());
            pooledDataSource.setPassword(propHolder.getPassword());

            pooledDataSource.setMinPoolSize(5);
            pooledDataSource.setAcquireIncrement(2);
            pooledDataSource.setMaxPoolSize(100);
        } catch (PropertyVetoException e) {
            e.printStackTrace();
        }
    }
}
