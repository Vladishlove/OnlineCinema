package ua.org.oa.neplokhv.cinema.dao.impl;

import ua.org.oa.neplokhv.cinema.dao.api.Dao;
import ua.org.oa.neplokhv.cinema.datasource.DataSource;
import ua.org.oa.neplokhv.cinema.model.Entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by Vlad on 9/12/2016.
 */
public abstract class CrudDao<T extends Entity> implements Dao<Integer, T> {

    private Class<T> type;
    private DataSource dataSource;

    public static final String SELECT_ALL = "Select * from %s";
    public static final String SELECT_ALL_ORDERED_BY = "Select * from %s order by %s";
    public static final String SELECT_ALL_BY_FOREIGN_KEY = "Select * from %s where %s_id%s = ?";
    private final String FIND_BY_ID = "Select * from %s where id%s = ?";
    private final String DELETE_BY_ID = "DELETE FROM %s WHERE id%s = ?";
    private final String FIND_BY_COLUMN_VALUE = "Select * from %s where %s = ?";

    public CrudDao(Class<T> type){
        this.type = type;
        dataSource = DataSource.getInstance();
    }

    @Override
    public List<T> getAll() {
        List<T> result;
        String sql = String.format(SELECT_ALL, type.getSimpleName().toLowerCase());
        result = executeSQL(sql);
        return result;
    }
    @Override
    public List<T> getAllOrderedBy(String colName) {
        List<T> result;
        String sql = String.format(SELECT_ALL_ORDERED_BY, type.getSimpleName().toLowerCase(), colName);
        result = executeSQL(sql);
        return result;
    }
    @Override
    public T getById(Integer key) throws NoSuchElementException{
        List result;
        String sql = String.format(FIND_BY_ID, type.getSimpleName().toLowerCase(), type.getSimpleName().toLowerCase());
        result = executeSQL(sql, key);
        if (result.isEmpty()) {
            throw new NoSuchElementException("element with id = " + key + " does non exist");
        }
        return (T)result.get(0);
    }

    @Override
    public List<T> getByForeignKey(Integer key, String entityName) {
        List<T> result = new ArrayList();
        String sql = String.format(SELECT_ALL_BY_FOREIGN_KEY, type.getSimpleName().toLowerCase(), entityName, entityName);
//        System.out.println(sql);
        result = executeSQL(sql, key);
        return result;
    }
    @Override
    public List<T> getByStringColumnValue(String columnName, String value) {
        List<T> result = new ArrayList();
        String sql = String.format(FIND_BY_COLUMN_VALUE, type.getSimpleName().toLowerCase(), columnName);
        try(Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, value);
            ResultSet rs = preparedStatement.executeQuery();
            result = readAll(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
    @Override
    public void save(T elem) {
        try(Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement =
                    createInsertPreparedStatement(connection, elem);
            preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                elem.setId(rs.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void update(T elem) {
        try(Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = createUpdatePreparedStatement(connection, elem);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void delete(Integer key) {
        String sql = String.format(DELETE_BY_ID, type.getSimpleName().toLowerCase(), type.getSimpleName().toLowerCase());
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, key);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    private List executeSQL(String sql, Integer key) {
        List result = new ArrayList();
        try(Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, key);
            ResultSet rs = preparedStatement.executeQuery();
            result = readAll(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
    private List executeSQL(String sql) {
        List result = new ArrayList();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet rs = preparedStatement.executeQuery();
            result = readAll(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public abstract List<T> readAll(ResultSet rs) throws SQLException;
    public abstract PreparedStatement createInsertPreparedStatement(Connection connection, T elem) throws SQLException;
    public abstract PreparedStatement createUpdatePreparedStatement(Connection connection, T elem) throws SQLException;
}