package ua.org.oa.neplokhv.cinema.service.impl;

import ua.org.oa.neplokhv.cinema.dao.DaoFactory;
import ua.org.oa.neplokhv.cinema.dao.api.Dao;
import ua.org.oa.neplokhv.cinema.dto.RoleDto;
import ua.org.oa.neplokhv.cinema.mapper.BeanMapper;
import ua.org.oa.neplokhv.cinema.model.Role;
import ua.org.oa.neplokhv.cinema.service.api.Service;

import java.util.List;

/**
 * Created by student on 13.09.2016.
 */
public class RoleServiceImpl implements Service<Integer, RoleDto> {

    private static RoleServiceImpl service;
    private Dao<Integer, Role> roleDao;
    private BeanMapper beanMapper;

    private RoleServiceImpl() {
        roleDao = DaoFactory.getInstance().getRoleDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized RoleServiceImpl getInstance() {
        if (service == null) {
            service = new RoleServiceImpl();
        }
        return service;
    }

    @Override
    public List<RoleDto> getAll() {
        List<Role> roles = roleDao.getAll();
        List<RoleDto> roleDtos = beanMapper.mapAll(roles, RoleDto.class);
        return roleDtos;
    }

    @Override
    public List<RoleDto> getAllOrderedBy(String colName) {
        return null;
    }

    @Override
    public List<RoleDto> getByFKey(Integer key, String entityName) {
        return null;
    }

    @Override
    public List<RoleDto> getByStringColumnValue(String columnName, String value) {
        return null;
    }

    @Override
    public RoleDto getById(Integer key) {
        RoleDto roleDto = beanMapper.singleMapper(roleDao.getById(key), RoleDto.class);
        return roleDto;
    }

    @Override
    public void save(RoleDto elem) {
        Role role = beanMapper.singleMapper(elem, Role.class);
        roleDao.save(role);
    }

    @Override
    public void delete(Integer key) {
        roleDao.delete(key);
    }

    @Override
    public void update(RoleDto elem) {
        Role role = beanMapper.singleMapper(elem, Role.class);
        roleDao.update(role);
    }
}