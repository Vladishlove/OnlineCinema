package ua.org.oa.neplokhv.cinema.dao.api;

import ua.org.oa.neplokhv.cinema.model.Entity;

import java.util.List;

/**
 * Created by Vlad on 9/12/2016.
 */
public interface Dao<K, T extends Entity> {
    List<T> getAll();
    List<T> getAllOrderedBy(String colName);
    List<T> getByForeignKey(Integer key, String entityName);
    List<T> getByStringColumnValue(String columnName, String value);
    T getById(K key);
    void save(T elem);
    void delete(K key);
    void update(T elem);
}
