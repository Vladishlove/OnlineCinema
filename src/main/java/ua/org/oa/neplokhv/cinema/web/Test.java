package ua.org.oa.neplokhv.cinema.web;
import static ua.org.oa.neplokhv.cinema.helpers.FileHelper.pathSeparator;

import org.dozer.DozerBeanMapper;
import ua.org.oa.neplokhv.cinema.dao.DaoFactory;
import ua.org.oa.neplokhv.cinema.dao.impl.SeanceDaoImpl;
import ua.org.oa.neplokhv.cinema.dto.*;
import ua.org.oa.neplokhv.cinema.helpers.PropertyHolder;
import ua.org.oa.neplokhv.cinema.model.Role;
import ua.org.oa.neplokhv.cinema.model.Seance;
import ua.org.oa.neplokhv.cinema.service.impl.*;

import java.io.File;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Vlad on 9/18/2016.
 */
public class Test {
    public static void main(String[] args) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY-MM-dd kk:HH:ss");
        LocalDateTime ldt = LocalDateTime.of(2016,10,22,10,00);
        String datetime = ldt.format(formatter);

//        DataSource ds = DataSource.getInstance();
//        String sql = "INSERT INTO " +
//                "cinema.seance(dateTime, Hall_idHall, movie_idMovie) " +
//                "VALUES (?,?,?)";
//        String sql2 = "SELECT * FROM seance";
//        try(Connection c = ds.getConnection()){
//            PreparedStatement ps = c.prepareStatement(sql2);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                Seance s = new Seance();
//                s.setId(rs.getInt("idseance"));
//                System.out.println(s.getId());
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
        //------Date test
//        MovieDto movieDto = MovieServiceImpl.getInstance().getById(7);
//        HallDto hallDto = HallServiceImpl.getInstance().getById(4);
//        HallDto hallDto2 = HallServiceImpl.getInstance().getById(5);
//        String dateTime = "2016-09-27T02:15:00";
//        dateTime = datetime.replace("T"," ");
//        Timestamp timestamp = new Timestamp(new Date().getTime());
//        Timestamp timestamp1 = Timestamp.valueOf("2016-10-01 10:00:00");
//        System.out.println(timestamp +" " + timestamp1);
//        Manager.createSeance(movieDto, hallDto2, timestamp);

//        ------GetSeance by movie id
//        String sql0 = "Select * from %s where %s_id%s = ?";
//        String sql = String.format(sql0, "seance", "movie", "movie");
//        List<Seance> seances = null;
//        try(Connection connection = DataSource.getInstance().getConnection()) {
//            PreparedStatement preparedStatement = connection.prepareStatement(sql);
//            preparedStatement.setInt(1, 7);
//            ResultSet rs = preparedStatement.executeQuery();
//            seances = new ArrayList<>();
//            Seance seance;
//            while (rs.next()) {
//                seance = new Seance();
//                seance.setId(rs.getInt("idseance"));
//                seance.setDateTime(rs.getTimestamp("dateTime"));
//                seance.setSoldTickets(DaoFactory.getInstance().getTicketDao().getByForeignKey(seance.getId(), "seance"));
//                seance.setHall(DaoFactory.getInstance().getHallDao().getById(rs.getInt("hall_idHall")));
//                seance.setMovie(DaoFactory.getInstance().getMovieDao().getById(rs.getInt("movie_idMovie")));
//                seances.add(seance);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        System.out.println(seances);

//        SeanceDto seanceDto = (SeanceDto) Manager.getSeancesByMovieID(7).get(0);
//        System.out.println(seanceDto.getDateTime().getDate());

        //---------Hall test
//        List<HallDto> hallDtos = HallServiceImpl.getInstance().getAll();
//        for (HallDto hallDto : hallDtos) {
//            System.out.println(hallDto.getId());
//        }
        //---Movie Test
//        List<String> mappingFiles = new ArrayList();
//        mappingFiles.add("C:\\Users\\Vlad\\IdeaProjects\\OnlineCinema\\src\\main\\resources\\dozerJdk8Converters.xml");
//        DozerBeanMapper dozerBeanMapper = new DozerBeanMapper();
//        dozerBeanMapper.setMappingFiles(mappingFiles);
//        System.out.println(SeanceServiceImpl.getInstance().getById(23));

        List<UserDto> userDtos = UserServiceImpl.getInstance().getAll();
        UserDto userDto = userDtos.get(0);
        userDto.setLastName("lastname");
        userDto.setLogin("ssssss");
        userDto.setPassword("ssssss");
        userDto.setEmail("ssssss");
        UserServiceImpl.getInstance().save(userDto);
        LocalDateTime ltd2 = LocalDateTime.now();
        ltd2.getHour();
        ldt.getMinute();
    }
}