package ua.org.oa.neplokhv.cinema.dao;


import ua.org.oa.neplokhv.cinema.dao.api.Dao;
import ua.org.oa.neplokhv.cinema.dao.impl.*;
import ua.org.oa.neplokhv.cinema.helpers.PropertyHolder;
import ua.org.oa.neplokhv.cinema.model.*;

/**
 * Created by tssupport15 on 13.09.16.
 */
public class DaoFactory {


    private static DaoFactory daoFactory = null;

    private Dao<Integer, Movie> movieDao;
    private Dao<Integer, User> userDao;
    private Dao<Integer, Hall> hallDao;
    private Dao<Integer, Role> roleDao;
    private Dao<Integer, Seance> seanceDao;
    private Dao<Integer, Ticket> ticketDao;

    private DaoFactory() {
        loadDaos();
    }

    public static DaoFactory getInstance() {
        if (daoFactory == null) {
            daoFactory = new DaoFactory();
        }
        return daoFactory;
    }

    private void loadDaos() {
        if (PropertyHolder.getInstance().isInMemory()) {

        }
        else {
            //System.out.println("in dao factory load method");
            movieDao = new MovieDaoImpl(Movie.class);
            roleDao = new RoleDaoImpl(Role.class);
            hallDao = new HallDaoImpl(Hall.class);
            seanceDao = new SeanceDaoImpl(Seance.class);
            ticketDao = new TicketDaoImpl(Ticket.class);
            userDao = new UserDaoImpl(User.class);
        }
    }

    public Dao<Integer, Movie> getMovieDao() {
        return movieDao;
    }
    public void setMovieDao(Dao<Integer, Movie> movieDao) {
        this.movieDao = movieDao;
    }
    public Dao<Integer, User> getUserDao() {
        return userDao;
    }
    public void setUserDao(Dao<Integer, User> userDao) {
        this.userDao = userDao;
    }
    public Dao<Integer, Hall> getHallDao() {
        return hallDao;
    }
    public void setHallDao(Dao<Integer, Hall> hallDao) {
        this.hallDao = hallDao;
    }
    public Dao<Integer, Role> getRoleDao() {
        return roleDao;
    }
    public void setRoleDao(Dao<Integer, Role> roleDao) {
        this.roleDao = roleDao;
    }

    public Dao<Integer, Seance> getSeanceDao() {
        return seanceDao;
    }

    public void setSeanceDao(Dao<Integer, Seance> seanceDao) {
        this.seanceDao = seanceDao;
    }

    public Dao<Integer, Ticket> getTicketDao() {
        return ticketDao;
    }

    public void setTicketDao(Dao<Integer, Ticket> ticketDao) {
        this.ticketDao = ticketDao;
    }
}
