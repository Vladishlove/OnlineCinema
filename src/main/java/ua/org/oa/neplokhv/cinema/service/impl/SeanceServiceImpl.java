package ua.org.oa.neplokhv.cinema.service.impl;

import ua.org.oa.neplokhv.cinema.dao.DaoFactory;
import ua.org.oa.neplokhv.cinema.dao.api.Dao;
import ua.org.oa.neplokhv.cinema.dto.SeanceDto;
import ua.org.oa.neplokhv.cinema.mapper.BeanMapper;
import ua.org.oa.neplokhv.cinema.model.Seance;
import ua.org.oa.neplokhv.cinema.service.api.Service;

import java.util.List;

/**
 * Created by student on 13.09.2016.
 */
public class SeanceServiceImpl implements Service<Integer, SeanceDto> {

    private static SeanceServiceImpl service;
    private Dao<Integer, Seance> seanceDao;
    private BeanMapper beanMapper;

    private SeanceServiceImpl() {
        seanceDao = DaoFactory.getInstance().getSeanceDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized SeanceServiceImpl getInstance() {
        if (service == null) {
            service = new SeanceServiceImpl();
        }
        return service;
    }

    @Override
    public List<SeanceDto> getAll() {
        List<Seance> seances = seanceDao.getAll();
        List<SeanceDto> seanceDtos = beanMapper.mapAll(seances, SeanceDto.class);
        return seanceDtos;
    }

    @Override
    public List<SeanceDto> getAllOrderedBy(String colName) {
        List<Seance> seances = seanceDao.getAllOrderedBy(colName);
        List<SeanceDto> seanceDtos = beanMapper.mapAll(seances, SeanceDto.class);
        return seanceDtos;
    }

    @Override
    public List<SeanceDto> getByFKey(Integer key, String  table) {
        List<Seance> seances = seanceDao.getByForeignKey(key, table);
        List<SeanceDto> seanceDtos = beanMapper.mapAll(seances, SeanceDto.class);
        return seanceDtos;
    }

    @Override
    public List<SeanceDto> getByStringColumnValue(String columnName, String value) {
        return null;
    }

    @Override
    public SeanceDto getById(Integer key) {
        SeanceDto seanceDto = beanMapper.singleMapper(seanceDao.getById(key), SeanceDto.class);
        return seanceDto;
    }

    @Override
    public void save(SeanceDto elem) {
        Seance seance = beanMapper.singleMapper(elem, Seance.class);
        seanceDao.save(seance);
    }

    @Override
    public void delete(Integer key) {
        seanceDao.delete(key);
    }

    @Override
    public void update(SeanceDto elem) {
        Seance seance = beanMapper.singleMapper(elem, Seance.class);
        seanceDao.update(seance);
    }
}