package ua.org.oa.neplokhv.cinema.service.api;

import java.util.List;

/**
 * Created by tssupport15 on 13.09.16.
 */
public interface Service<K, T> {
    List<T> getAll();
    List<T> getAllOrderedBy(String colName);
    List<T> getByFKey(K key, String entityName);
    List<T> getByStringColumnValue(String columnName, String value);
    T getById(K key);
    void save(T elem);
    void delete(K key);
    void update(T elem);
}
