package ua.org.oa.neplokhv.cinema.controllers;


import ua.org.oa.neplokhv.cinema.dto.MovieDto;
import ua.org.oa.neplokhv.cinema.service.impl.MovieServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;

import static ua.org.oa.neplokhv.cinema.helpers.FileHelper.pathSeparator;

/**
 * Created by Vlad on 9/27/2016.
 */
public class EditMovieServlet extends HttpServlet {
    @SuppressWarnings("Duplicates")
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        MovieServiceImpl movieService = MovieServiceImpl.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        boolean dataFilled;
        MovieDto movieDto = (MovieDto) request.getSession().getAttribute("movie");
        try {
            movieDto.setMovieName(request.getParameter("movieName"));
            movieDto.setDuration(Integer.parseInt(request.getParameter("duration")));
            movieDto.setDescription(request.getParameter("description"));
            movieDto.setStartDate(
                    LocalDateTime.of(
                            LocalDate.parse(request.getParameter("startDate")), LocalTime.MIN));
            movieDto.setEndDate(
                    LocalDateTime.of(
                            LocalDate.parse(request.getParameter("endDate")), LocalTime.MIN));
            dataFilled = true;
        } catch (NullPointerException | NumberFormatException e) {
            dataFilled = false;
            request.setAttribute("error", e.getMessage());
            request.getRequestDispatcher(pathSeparator+
                            "pages"+pathSeparator+"admin"+pathSeparator+"editMovie.jsp").
                    forward(request,response);
        }
        if (dataFilled) {
            movieService.update(movieDto);
        }
        request.getRequestDispatcher(pathSeparator+
                        "pages"+pathSeparator+"admin"+pathSeparator+"movie.jsp").
                forward(request, response);
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getSession().setAttribute("movie", MovieServiceImpl.
                getInstance().getById(Integer.parseInt(request.getParameter("id"))));
        response.sendRedirect(getServletContext().getContextPath()+pathSeparator+
                "pages"+pathSeparator+"admin"+pathSeparator+"editMovie.jsp");
    }
}