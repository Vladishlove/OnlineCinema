package ua.org.oa.neplokhv.cinema.web;

import ua.org.oa.neplokhv.cinema.dao.DaoFactory;
import ua.org.oa.neplokhv.cinema.dao.impl.RoleDaoImpl;
import ua.org.oa.neplokhv.cinema.dao.impl.TicketDaoImpl;
import ua.org.oa.neplokhv.cinema.dao.impl.UserDaoImpl;
import ua.org.oa.neplokhv.cinema.dto.*;
import ua.org.oa.neplokhv.cinema.model.Role;
import ua.org.oa.neplokhv.cinema.model.User;
import ua.org.oa.neplokhv.cinema.service.api.Service;
import ua.org.oa.neplokhv.cinema.service.impl.*;

import java.sql.SQLException;

/**
 * Created by Vlad on 9/12/2016.
 */
public class Main {
    public static void main(String[] args) throws SQLException {
//        final String DELETE_BY_ID = "DELETE FROM cinema.movie WHERE movie_id = ?";
//        Service movieService = MovieServiceImpl.getInstance();

//        Service userService = UserServiceImpl.getInstance();
//        TicketDaoImpl ticketDao = TicketDaoImpl.getInstance();



//        Service hallService = HallServiceImpl.getInstance();
//        Service roleService = RoleServiceImpl.getInstance();
//        Service seanceService = SeanceServiceImpl.getInstance();
//        Service ticketService = TicketServiceImpl.getInstance();

        //----------------Movie
//        movieService.save(new MovieDto("Movie test", "description of the film", 120));
//        movieService.save(new MovieDto("Movie test2", "Movie is the best of the best", 240));
//        MovieDto mdt = (MovieDto) movieService.getAll().get(0);
//        MovieDto movie = (MovieDto) movieService.getById(6);
//        movie.setMovieName("7-th ronin");
//        movie.setDescription("ronin");
//        movie.setDuration(1111);
//        movieService.update(movie);
//        movieService.getAll().forEach(System.out::println);

        //-------------Role

//        RoleDaoImpl role = (RoleDaoImpl) DaoFactory.getInstance().getRoleDao();
        //roleService.save(new RoleDto("Admin"));
        //System.out.println(roleService.getAll());

        //------------User
//        UserServiceImpl userService = UserServiceImpl.getInstance();
//        System.out.println(userService.getAll());
        //System.out.println(userService.getByStringColumnValue("login", "vasiol"));
//        userService.save(new UserDto("Petya", "Pupkin", "vasiol", "pass", "email"));
//        UserDto userDto = (UserDto) userService.getById(13);
//        userDto.setRole(new Role("User"));
//        userService.update(userDto);
//        userService.delete(9);
//        userDto.setEmail("new email");
//        userService.update(userDto);
//        System.out.println("GetById - "+ movieService.getById(4));
//        userService.getAll().forEach(System.out::println);
//        System.out.println("GetUserById - "+ userService.getById(1));

//-----------Hall----------
        //hallService.save(new HallDto("Second", 20, 10));
//        System.out.println(hallService.getAll());
//        hallService.delete(1);
//        HallDto hallToUpdate = (HallDto) hallService.getById(2);
//        hallToUpdate.setPlaceQuantity(25);
//        hallService.update(hallToUpdate);
//        System.out.println(hallService.getAll());
        //System.out.println(hallDto.getName());
        //HallDto hallDto = (HallDto) hallService.getById(9);

        //-----------Seance

//        seanceDto.setMovie((MovieDto) movieService.getById(6));

//        seanceDto.setHall((HallDto)hallService.getById(3));
//        seanceDto.setMovie((MovieDto)movieService.getById(4));
//        LocalDateTime ldt = LocalDateTime.now();
//        seanceDto.setDateTime(Timestamp.valueOf(ldt));
//        seanceService.save(seanceDto);
//        System.out.println(seanceService.getAll());


        //------------Ticket

//        UserDto user = (UserDto) userService.getById(12);
//        TicketServiceImpl ticketService = TicketServiceImpl.getInstance();
//        ticketService.save(new TicketDto(5, 6, 17, user));
//
//        TicketDto ticketDto = (TicketDto)ticketService.getById(1);
//        TicketDto ticketDto2 = (TicketDto)ticketService.getById(3);
//
//        ticketDto.setSold(true);
//        ticketDto2.setSold(true);
//        ticketService.update(ticketDto);
//        ticketService.update(ticketDto2);

//        ticketService.delete(1);
        //System.out.println(ticketService.getAll());


        //-----------Ticket buying

//        Manager.buyTicket(new TicketDto(), 1, 1, (SeanceDto) seanceService.getById(17), (UserDto)userService.getById(12));

//        UserDto userDto = (UserDto) userService.getById(13);
//
//        seanceDto.viewPlacesMap();
//
//        TicketDto ticketDto = new TicketDto(4, 2, seanceDto.getId(), userDto);
//        seanceDto.setOccupied(ticketDto.getPlace(),ticketDto.getRow());
//        ticketDto.setSold(true);
//        ticketService.save(ticketDto);
//        System.out.println();
//        seanceDto.viewPlacesMap();
    }
}