package ua.org.oa.neplokhv.cinema.dto;

import ua.org.oa.neplokhv.cinema.model.Entity;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * Created by tssupport15 on 13.09.16.
 */
public class MovieDto extends Entity{

    private String movieName;
    private String description;
    private String country;
    private String cast;
    private String director;
    private String writer;
    private LocalDateTime openingDate;
    private int duration;
    private boolean available;
    private LocalDateTime startDate;
    private LocalDateTime endDate;

    @Override
    public String toString() {
        return "MovieDto{" + "id=" + getId() +", "+
                "movieName='" + movieName + '\'' +
                ", description='" + description + '\'' +
                ", duration=" + duration +
                '}';
    }

    public MovieDto() {
    }
    public MovieDto(String fn, String d, int dur) {
        setMovieName(fn);
        setDescription(d);
        setDuration(dur);
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        if (movieName == null && movieName.equals("")) {
            throw new NullPointerException("movie name can not be empty");
        }
        this.movieName = movieName;
    }

    public boolean isAvailable() {
        return endDate.compareTo(LocalDateTime.now())>=0;
    }
    public void setAvailable(boolean avaliable) {
        this.available = avaliable;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        if (duration == 0) {
            throw new NullPointerException("duration can not be 0");
        }
        this.duration = duration;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCast() {
        return cast;
    }

    public void setCast(String cast) {
        this.cast = cast;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public LocalDateTime getOpeningDate() {
        return openingDate;
    }

    public void setOpeningDate(LocalDateTime openingDate) {
        this.openingDate = openingDate;
    }
}