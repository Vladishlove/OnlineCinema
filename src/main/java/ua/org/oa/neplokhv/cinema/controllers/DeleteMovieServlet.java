package ua.org.oa.neplokhv.cinema.controllers;


import ua.org.oa.neplokhv.cinema.dto.MovieDto;
import ua.org.oa.neplokhv.cinema.service.impl.MovieServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static ua.org.oa.neplokhv.cinema.helpers.FileHelper.pathSeparator;

/**
 * Created by Vlad on 9/27/2016.
 */
public class DeleteMovieServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        MovieServiceImpl movieService = MovieServiceImpl.getInstance();
        movieService.delete(Integer.parseInt(request.getParameter("id")));
        //Updating movieList
        getServletContext().setAttribute("movies", MovieServiceImpl.getInstance().getAll());
        request.getRequestDispatcher("/movieList")
                .forward(request, response);
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }
}