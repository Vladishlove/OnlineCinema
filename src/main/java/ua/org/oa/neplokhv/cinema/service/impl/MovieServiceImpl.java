package ua.org.oa.neplokhv.cinema.service.impl;

import ua.org.oa.neplokhv.cinema.dao.DaoFactory;
import ua.org.oa.neplokhv.cinema.dao.api.Dao;
import ua.org.oa.neplokhv.cinema.dto.MovieDto;
import ua.org.oa.neplokhv.cinema.mapper.BeanMapper;
import ua.org.oa.neplokhv.cinema.model.Movie;
import ua.org.oa.neplokhv.cinema.service.api.Service;

import java.util.List;

/**
 * Created by student on 13.09.2016.
 */
public class MovieServiceImpl implements Service<Integer, MovieDto> {

    private static MovieServiceImpl service;
    private Dao<Integer, Movie> movieDao;
    private BeanMapper beanMapper;

    private MovieServiceImpl() {
        movieDao = DaoFactory.getInstance().getMovieDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized MovieServiceImpl getInstance() {
        if (service == null) {
            service = new MovieServiceImpl();
        }
        return service;
    }

    @Override
    public List<MovieDto> getAll() {
        List<Movie> movies = movieDao.getAll();
        List<MovieDto> movieDtos = beanMapper.mapAll(movies, MovieDto.class);
        return movieDtos;
    }

    @Override
    public List<MovieDto> getAllOrderedBy(String colName) {
        return null;
    }

    @Override
    public List<MovieDto> getByFKey(Integer key, String entityName) {
        return null;
    }

    @Override
    public List<MovieDto> getByStringColumnValue(String columnName, String value) {
        return null;
    }

    @Override
    public MovieDto getById(Integer key) {
        MovieDto movieDto = beanMapper.singleMapper(movieDao.getById(key), MovieDto.class);
        return movieDto;
    }

    @Override
    public void save(MovieDto elem) {
        Movie movie = beanMapper.singleMapper(elem, Movie.class);
        movieDao.save(movie);
    }

    @Override
    public void delete(Integer key) {
        movieDao.delete(key);
    }

    @Override
    public void update(MovieDto elem) {
        Movie movie = beanMapper.singleMapper(elem, Movie.class);
        movieDao.update(movie);
    }
}