package ua.org.oa.neplokhv.cinema.dao.impl;


import ua.org.oa.neplokhv.cinema.model.Role;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vlad on 9/18/2016.
 */
public class RoleDaoImpl extends CrudDao<Role>{

    private final String INSERT = "INSERT INTO " +
            "cinema.role(roleName) " +
            "VALUES (?)";
    private final String UPDATE = "UPDATE cinema.role " +
            "SET roleName = ?";

    private static RoleDaoImpl roleDao;
    public RoleDaoImpl(Class type) {
        super(type);
    }

    public static synchronized RoleDaoImpl getInstance() {
        if (roleDao == null) {
            roleDao = new RoleDaoImpl(Role.class);
        }
        return roleDao;
    }

    @Override
    public PreparedStatement createInsertPreparedStatement(Connection connection, Role elem)
            throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, elem.getName());
        return preparedStatement;
    }
    @Override
    public PreparedStatement createUpdatePreparedStatement(Connection connection, Role elem)
            throws SQLException{
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        preparedStatement.setString(1, elem.getName());
        return preparedStatement;
    }
    @Override
    public List<Role> readAll(ResultSet rs) throws SQLException {
        List<Role> roles = new ArrayList<>();
        Role role;
        while (rs.next()) {
            role = new Role(rs.getString("roleName"));
            role.setId(rs.getInt("idrole"));
            roles.add(role);
        }
        return roles;
    }
}