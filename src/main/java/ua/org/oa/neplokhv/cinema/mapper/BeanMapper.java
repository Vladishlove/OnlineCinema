package ua.org.oa.neplokhv.cinema.mapper;

import org.dozer.DozerBeanMapper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tssupport15 on 13.09.16.
 */
public class BeanMapper {
    private static BeanMapper beanMapper = null;
    private static DozerBeanMapper mapper = null;

    private BeanMapper() {
        List<String> mappingFiles = new ArrayList();
        mappingFiles.add(BeanMapper.class.getClassLoader().getResource("dozerJdk8Converters.xml").toString());
        mapper = new DozerBeanMapper();
        mapper.setMappingFiles(mappingFiles);
    }

    public static synchronized BeanMapper getInstance() {
        if (beanMapper == null) {
            beanMapper = new BeanMapper();
        }
        return beanMapper;
    }

    public static <T> T singleMapper(Object from, Class<T> toClass) {
        T map = mapper.map(from, toClass);
        return map;
    }
    public static <E, T> List<T> mapAll(Iterable<E> iterable, Class<T> toClass) {
        List<T> list = new ArrayList<>();
        for (E e : iterable) {
            list.add(mapper.map(e, toClass));
        }

        return list;
    }

}