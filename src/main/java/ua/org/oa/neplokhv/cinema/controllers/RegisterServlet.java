package ua.org.oa.neplokhv.cinema.controllers;


import ua.org.oa.neplokhv.cinema.dto.RoleDto;
import ua.org.oa.neplokhv.cinema.dto.UserDto;
import ua.org.oa.neplokhv.cinema.model.Role;
import ua.org.oa.neplokhv.cinema.service.impl.UserServiceImpl;
import ua.org.oa.neplokhv.cinema.web.Manager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import static ua.org.oa.neplokhv.cinema.helpers.FileHelper.pathSeparator;

/**
 * Created by Vlad on 9/27/2016.
 */
public class RegisterServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserServiceImpl userService = UserServiceImpl.getInstance();
        if (request.getSession().getAttribute("user") != null) {
            request.getRequestDispatcher(getServletContext().getContextPath()+pathSeparator+"index.jsp")
                    .forward(request,response);
        }
        UserDto user = new UserDto();
        HttpSession session = request.getSession();
        try {
            user.setName(request.getParameter("firstName"));
            session.setAttribute("firstName", user.getName());

            user.setLastName(request.getParameter("lastName"));
            session.setAttribute("lastName", user.getLastName());

            user.setLogin(request.getParameter("login"));
            session.setAttribute("login", user.getLogin());

            user.setEmail(request.getParameter("email"));
            session.setAttribute("email", user.getEmail());

            if (request.getParameter("password").equals(request.getParameter("passwordS"))){
                user.setPassword(request.getParameter("password"));
            }
            else user.setPassword("");
            user.setRole(new RoleDto("user"));
            Manager.saveUser(user);
            request.getSession().setAttribute("user", user);
            if (request.getSession().getAttribute("urlToReturn") == null) {
                response.sendRedirect(getServletContext().
                        getContextPath()+
                        pathSeparator+"pages"+
                        pathSeparator+user.getRole().getName()+
                        pathSeparator+"movies.jsp");
            }
            else {
                String urlToReturnStr = (String) request.getSession().getAttribute("urlToReturn");
                response.sendRedirect(urlToReturnStr.replace("common",user.getRole().getName()));
            }
        } catch (IllegalArgumentException e) {
            request.getSession().setAttribute("userRegistrationMessage", e.getMessage());
            response.sendRedirect(getServletContext().
                            getContextPath()+
                    pathSeparator+"pages"+
                    pathSeparator+"common"+
                    pathSeparator+"registration.jsp");
            e.printStackTrace();
        }
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getSession().getAttribute("user") != null) {
            UserDto userDto = (UserDto) request.getSession().getAttribute("user");
            response.sendRedirect(getServletContext().
                    getContextPath()+
                    pathSeparator+"pages"+
                    pathSeparator+ userDto.getRole().getName()+
                    pathSeparator+"movies.jsp");
        }
        else {
            response.sendRedirect(getServletContext().getContextPath()+
                    pathSeparator+"pages"+
                    pathSeparator+"common"+
                    pathSeparator+"registration.jsp");
        }
    }
}