package ua.org.oa.neplokhv.cinema.web;

import ua.org.oa.neplokhv.cinema.dto.*;
import ua.org.oa.neplokhv.cinema.model.Hall;
import ua.org.oa.neplokhv.cinema.service.api.Service;
import ua.org.oa.neplokhv.cinema.service.impl.*;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by tssupport15 on 20.09.16.
 */
public class Manager {
    public static synchronized void buyTicket(int row, int place, SeanceDto seanceDto, UserDto userDto) {
        Service ticketService = TicketServiceImpl.getInstance();
        TicketDto ticketDto = new TicketDto();

        ticketDto.setRow(row);
        ticketDto.setPlace(place);
        ticketDto.setSeanceId(seanceDto.getId());
        ticketDto.setSold(true);
        ticketDto.setUserId(userDto.getId());

        ticketService.save(ticketDto);
    }

    public static void updateUser(UserDto userDto) throws IllegalArgumentException{
        if (isUserDataFilled(userDto))
        UserServiceImpl.getInstance().update(userDto);
    }
    public static void saveUser(UserDto userDto) throws IllegalArgumentException{
        if (isUserDataFilled(userDto))
        UserServiceImpl.getInstance().save(userDto);
    }
    public static boolean isUnicValue(UserDto userDto, String login, String email) throws IllegalArgumentException{
        for (UserDto user : UserServiceImpl.getInstance().getAll()) {
            if (user.getEmail().equals(email)) {
                if (user.getEmail().equals(userDto.getEmail())) continue;
                throw new IllegalArgumentException("Email already exists");
            }
            if (user.getLogin().equals(login)){
                if (user.getLogin().equals(userDto.getLogin())) continue;
                throw new IllegalArgumentException("Login already exists");
            }
        }
        return true;
    }
    public static boolean isUserDataFilled(UserDto userDto) throws IllegalArgumentException{
        if (userDto.getLogin().isEmpty() || userDto.getLogin() == null)
            throw new IllegalArgumentException("Login can not be empty");

        if (userDto.getEmail().isEmpty() || userDto.getEmail() == null)
            throw new IllegalArgumentException("Email can not be empty");

        if (userDto.getPassword().equals("") || userDto.getPassword() == null){
            throw new IllegalArgumentException("Please check password field");
        }
        return true;
    }

    public static List<SeanceDto> getSeancesByMovieID(int id) {
        List<SeanceDto> seance = SeanceServiceImpl.getInstance().getByFKey(id,"movie");
        Collections.sort(seance,new Comparator<SeanceDto>(){
            @Override
            public int compare(SeanceDto o1, SeanceDto o2) {
                return o1.getDateTime().compareTo(o2.getDateTime());
            }
        });
        return seance;
    }

    public static void createSeance(MovieDto movieDto, HallDto hallDto, LocalDateTime dateTime, int ticketPrice) {
        SeanceDto seanceDto = fillSeanceData(new SeanceDto(), movieDto, hallDto, dateTime, ticketPrice);


        SeanceServiceImpl.getInstance().save(seanceDto);
    }

    public static void updateSeance(SeanceDto seanceDto, MovieDto movieDto, HallDto hallDto, LocalDateTime dateTime, int ticketPrice) {
        seanceDto = fillSeanceData(seanceDto, movieDto, hallDto, dateTime, ticketPrice);
        SeanceServiceImpl.getInstance().
                update(seanceDto);
    }

    public static SeanceDto fillSeanceData (SeanceDto seanceDto, MovieDto movieDto, HallDto hallDto, LocalDateTime dateTime, int ticketPrice) {

        SeanceServiceImpl seanceService = SeanceServiceImpl.getInstance();
        List<SeanceDto> seances = seanceService.getAll();
        int filmDuration = movieDto.getDuration();
        for (SeanceDto seance : seances) {
            LocalDateTime ts = seance.getDateTime();
            if (!isValidSeanceTime(ts, dateTime, filmDuration) &&
                    seance.getHall().getName().equals(hallDto.getName())) {
                if (seance.getId() == seanceDto.getId()){
                    continue;
                }
                else throw new IllegalArgumentException
                        ("At this time hall is occupied");

            }
        }
        seanceDto.setDateTime(dateTime);
        System.out.println("OK, I can save it to DB");
        if (hallDto == null) throw new IllegalArgumentException("hall can not be null");
        seanceDto.setHall(hallDto);
        if (movieDto == null) throw new IllegalArgumentException("movie can not be null");
        seanceDto.setMovie(movieDto);
        if (seanceDto.getDateTime().isBefore(movieDto.getStartDate()) ||
        seanceDto.getDateTime().isAfter(movieDto.getEndDate().plusDays(1))) {
            throw new IllegalArgumentException("Illegal seance time for this movie");
        }
        if (ticketPrice <= 0) throw new IllegalArgumentException("price can not be 0");
        seanceDto.setTicketPrice(ticketPrice);
        return seanceDto;
    }

    public static boolean isValidSeanceTime(LocalDateTime ts, LocalDateTime dateTime, int duration) {
        if (ts.getYear() != dateTime.getYear()) return true;
        if (ts.getMonth() != dateTime.getMonth()) return true;
        if (ts.getDayOfMonth() != dateTime.getDayOfMonth()) return true;
        if (ts.getYear() == dateTime.getYear() &&
                ts.getMonth() == dateTime.getMonth() &&
                ts.getDayOfMonth() == dateTime.getDayOfMonth() &&
                ts.getHour() == dateTime.getHour()) {
            return false;
        }
        int hBetween =
                Math.abs((dateTime.getHour()*60+dateTime.getMinute()) - (ts.getHour()*60+ts.getMinute()));
        System.out.println(hBetween);
        return hBetween > duration + 15;
    }

    public static UserDto fillUserData(String login, String password, String role) {
        UserDto userDto = new UserDto();
        RoleDto roleDto = new RoleDto();
        roleDto.setName(role);
        RoleServiceImpl.getInstance().save(roleDto);
        userDto.setLogin(login);
        userDto.setPassword(password);
        userDto.setRole(roleDto);
        System.out.println(userDto);
        return userDto;
    }
    public static void createHall(String hallName, int rowQuantity, int placeQuantity)
            throws IllegalArgumentException {
        if (hallName == null && hallName.isEmpty()) throw new IllegalArgumentException("empty hall name");
        if (rowQuantity <= 0 && placeQuantity <= 0) throw new IllegalArgumentException("illegal hall size");
        HallDto hallDto = new HallDto();
        hallDto.setName(hallName);
        hallDto.setRowQuantity(rowQuantity);
        hallDto.setPlaceQuantity(placeQuantity);
        HallServiceImpl.getInstance().save(hallDto);
    }
    public static String viewTicket(TicketDto ticketDto) {
        StringBuilder stringBuilder = new StringBuilder();
        SeanceDto seanceDto = SeanceServiceImpl.getInstance().getById(ticketDto.getSeanceId());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd:MM:yyyy HH:mm");
        stringBuilder.append("Date: ").append(seanceDto.getDateTime().format(formatter)).
                append(", Hall: ").append(seanceDto.getHall().getName()).
                append(", Movie: ").append(seanceDto.getMovie().getMovieName()).
                append(", Price: ").append(seanceDto.getTicketPrice()).
                append(", Row: ").append(ticketDto.getRow()).
                append(", Place: ").append(ticketDto.getPlace()).append(System.getProperty("line.separator"));
        return stringBuilder.toString();
    }
}