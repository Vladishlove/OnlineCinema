package ua.org.oa.neplokhv.cinema.dao.impl;

import ua.org.oa.neplokhv.cinema.dao.DaoFactory;
import ua.org.oa.neplokhv.cinema.model.Role;
import ua.org.oa.neplokhv.cinema.model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tssupport15 on 13.09.16.
 */
public class UserDaoImpl extends CrudDao<User> {

    private final String INSERT = "INSERT INTO " +
            "cinema.user(login, password, email, name, lastName, role_idrole) " +
            "VALUES (?,?,?,?,?,?)";
    private final String UPDATE = "UPDATE cinema.user SET " +
            "login = ?, password = ?, email = ?, name = ?, lastName = ?, role_idrole = ? " +
            "WHERE iduser = ?";

    private static UserDaoImpl crudDao;
    private static TicketDaoImpl ticketDao;
    private static RoleDaoImpl roleDao;

    public UserDaoImpl(Class type) {
        super(type);
    }

    public static synchronized UserDaoImpl getInstance() {
        if (crudDao == null) {
            crudDao = new UserDaoImpl(User.class);
        }
        return crudDao;
    }
    @Override
    public List<User> readAll(ResultSet rs) throws SQLException {
        ticketDao = (TicketDaoImpl) DaoFactory.getInstance().getTicketDao();
        roleDao = (RoleDaoImpl) DaoFactory.getInstance().getRoleDao();
        List<User> users = new ArrayList<>();
        User user;
        while (rs.next()) {
            user = new User();
            user.setId(rs.getInt("idUser"));
            user.setName((rs.getString("name")));
            user.setLastName(rs.getString("lastName"));
            user.setLogin(rs.getString("login"));
            user.setPassword(rs.getString("password"));
            user.setEmail(rs.getString("email"));
            user.setTicketList(ticketDao.getByForeignKey(user.getId(),"user"));
            user.setRole(roleDao.getById(rs.getInt("role_idrole")));
            users.add(user);
        }
        return users;
    }
    @Override
    public PreparedStatement createInsertPreparedStatement(Connection connection, User elem) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, elem.getLogin());
        preparedStatement.setString(2, elem.getPassword());
        preparedStatement.setString(3, elem.getEmail());
        preparedStatement.setString(4, elem.getName());
        preparedStatement.setString(5, elem.getLastName());
        preparedStatement.setInt(6, getRoleId(elem));
        return preparedStatement;
    }

    @Override
    public PreparedStatement createUpdatePreparedStatement(Connection connection, User elem) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        preparedStatement.setString(1, elem.getLogin());
        preparedStatement.setString(2, elem.getPassword());
        preparedStatement.setString(3, elem.getEmail());
        preparedStatement.setString(4, elem.getName());
        preparedStatement.setString(5, elem.getLastName());
        preparedStatement.setInt(6, getRoleId(elem));
        preparedStatement.setInt(7, elem.getId());
        return preparedStatement;
    }
    private int getRoleId(User elem) {
        System.out.println("in get role ID");
        List<Role> roles = DaoFactory.getInstance().getRoleDao().getAll();
        int roleID = 0;
        for (Role role : roles) {
            if (role.getName().equals(elem.getRole().getName())) {
                roleID = role.getId();
            }
        }
        return roleID;
    }
}