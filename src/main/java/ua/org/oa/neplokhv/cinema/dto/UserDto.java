package ua.org.oa.neplokhv.cinema.dto;

import ua.org.oa.neplokhv.cinema.dao.impl.RoleDaoImpl;
import ua.org.oa.neplokhv.cinema.model.Entity;
import ua.org.oa.neplokhv.cinema.model.Role;
import ua.org.oa.neplokhv.cinema.service.impl.UserServiceImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by tssupport15 on 13.09.16.
 */
public class UserDto extends Entity{

    private String name;
    private String lastName;
    private String login;
    private String password;
    private String email;
    private List<TicketDto> ticketList;
    private RoleDto role;

    public UserDto(){
        ticketList = new ArrayList<>();
    }
    public UserDto(String name, String lastName, String login, String password, String email){
        setName(name);
        setLastName(lastName);
        setLogin(login);
        setPassword(password);
        setEmail(email);
        ticketList = new ArrayList<>();
        role = new RoleDto("User");
    }
    public UserDto(String name, String login, String password, RoleDto r){
        setName(name);
        setLogin(login);
        setPassword(password);
        ticketList = new ArrayList<>();
        role = r;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void addTicket(TicketDto t) {
        ticketList.add(t);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLogin() {

        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<TicketDto> getTicketList() {
        return ticketList;
    }

    public void setTicketList(List<TicketDto> ticketList) {
        this.ticketList = ticketList;
    }

    public RoleDto getRole() {
        return role;
    }

    public void setRole(RoleDto role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + getId() + ", " +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", ticketList=" + ticketList +
                ", role=" + role +
                '}';
    }
}