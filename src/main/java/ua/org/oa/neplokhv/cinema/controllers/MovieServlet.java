package ua.org.oa.neplokhv.cinema.controllers;

import ua.org.oa.neplokhv.cinema.dto.MovieDto;
import ua.org.oa.neplokhv.cinema.dto.SeanceDto;
import ua.org.oa.neplokhv.cinema.dto.UserDto;
import ua.org.oa.neplokhv.cinema.helpers.DateNavigation;
import ua.org.oa.neplokhv.cinema.service.impl.MovieServiceImpl;
import ua.org.oa.neplokhv.cinema.service.impl.SeanceServiceImpl;
import ua.org.oa.neplokhv.cinema.service.impl.UserServiceImpl;
import ua.org.oa.neplokhv.cinema.web.Manager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static ua.org.oa.neplokhv.cinema.helpers.FileHelper.pathSeparator;

/**
 * Created by Vlad on 9/27/2016.
 */
public class MovieServlet extends HttpServlet{
    @SuppressWarnings("Duplicates")
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LocalDateTime selectedDate;
        LocalDateTime sessionAttr = (LocalDateTime) request.getSession().getAttribute("selected");
        String requestParameter = request.getParameter("selected");
        selectedDate = DateNavigation.setSelectedDate(sessionAttr, requestParameter);
        request.getSession().setAttribute("selected", selectedDate);

        String idStr = request.getParameter("id");
        List<LocalDateTime> dateList = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            dateList.add(LocalDateTime.now().plusDays(i));
        }
        try {
            int id = Integer.parseInt(idStr);
            MovieDto movie = MovieServiceImpl.getInstance().
                    getById(id);
            List<SeanceDto> seanceDtos = Manager.getSeancesByMovieID(id);
            request.setAttribute("movie", movie);
            request.setAttribute("seances", seanceDtos);
            request.getSession().setAttribute("dates", dateList);
            UserDto userDto = (UserDto) request.getSession().getAttribute("user");
            if (userDto != null) {
                request.getRequestDispatcher(
                        "pages" + pathSeparator +
                                userDto.getRole().getName() + pathSeparator +
                                "movie.jsp").forward(request, response);
            }
            else {
                request.getRequestDispatcher("pages" + pathSeparator +
                        "common"+pathSeparator+"movie.jsp").forward(request,response);
            }


        } catch (NumberFormatException | NoSuchElementException e) {
            response.sendRedirect("pages" + pathSeparator +
                    "common"+pathSeparator+"sorry.jsp");
        }

        }
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }
}