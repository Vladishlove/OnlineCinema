package ua.org.oa.neplokhv.cinema.model;


/**
 * Created by vlad on 12.08.16.
 */
public class Role extends Entity {

    private String name;
    private static final String admin = "admin";
    private static final String user = "user";

    public Role(){}

    public Role(String name) {
        setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.equals(admin) || name.equals(user)){
            this.name = name;

        }
        else throw new IllegalArgumentException("not supported role name");
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}