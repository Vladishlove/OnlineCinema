package ua.org.oa.neplokhv.cinema.service.impl;

import ua.org.oa.neplokhv.cinema.dao.DaoFactory;
import ua.org.oa.neplokhv.cinema.dao.api.Dao;
import ua.org.oa.neplokhv.cinema.dto.TicketDto;
import ua.org.oa.neplokhv.cinema.mapper.BeanMapper;
import ua.org.oa.neplokhv.cinema.model.Ticket;
import ua.org.oa.neplokhv.cinema.service.api.Service;

import java.util.List;

/**
 * Created by student on 13.09.2016.
 */
public class TicketServiceImpl implements Service<Integer, TicketDto> {

    private static TicketServiceImpl service;
    private Dao<Integer, Ticket> ticketDao;
    private BeanMapper beanMapper;

    private TicketServiceImpl() {
        ticketDao = DaoFactory.getInstance().getTicketDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized TicketServiceImpl getInstance() {
        if (service == null) {
            service = new TicketServiceImpl();
        }
        return service;
    }

    @Override
    public List<TicketDto> getAll() {
        List<Ticket> tickets = ticketDao.getAll();
        List<TicketDto> ticketDtos = beanMapper.mapAll(tickets, TicketDto.class);
        return ticketDtos;
    }

    @Override
    public List<TicketDto> getAllOrderedBy(String colName) {
        return null;
    }

    @Override
    public List<TicketDto> getByFKey(Integer key, String entityName) {
        List<Ticket> tickets = ticketDao.getByForeignKey(key, entityName);
        List<TicketDto> ticketDtos = beanMapper.mapAll(tickets, TicketDto.class);
        return ticketDtos;
    }

    @Override
    public List<TicketDto> getByStringColumnValue(String columnName, String value) {
        return null;
    }

    @Override
    public TicketDto getById(Integer key) {
        TicketDto ticketDto = beanMapper.singleMapper(ticketDao.getById(key), TicketDto.class);
        return ticketDto;
    }

    @Override
    public void save(TicketDto elem) {
        Ticket ticket = beanMapper.singleMapper(elem, Ticket.class);
        ticketDao.save(ticket);
    }

    @Override
    public void delete(Integer key) {
        ticketDao.delete(key);
    }

    @Override
    public void update(TicketDto elem) {
        Ticket ticket = beanMapper.singleMapper(elem, Ticket.class);
        ticketDao.update(ticket);
    }
}