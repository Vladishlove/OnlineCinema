package ua.org.oa.neplokhv.cinema.dto;

import ua.org.oa.neplokhv.cinema.model.Entity;

/**
 * Created by tssupport15 on 16.09.16.
 */
public class HallDto extends Entity {

    private int rowQuantity;
    private int placeQuantity;
    private String name;

    public HallDto() {

    }

    public int getRowQuantity() {
        return rowQuantity;
    }
    public void setRowQuantity(int rowQuantity) {
        this.rowQuantity = rowQuantity;
    }
    public int getPlaceQuantity() {
        return placeQuantity;
    }
    public void setPlaceQuantity(int placeQuantity) {
        this.placeQuantity = placeQuantity;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "HallDto{" + "id=" + getId() +
                "rowQuantity=" + rowQuantity +
                ", placeQuantity=" + placeQuantity +
                ", name='" + name + '\'' +
                '}';
    }
}