package ua.org.oa.neplokhv.cinema.service.impl;

import ua.org.oa.neplokhv.cinema.dao.DaoFactory;
import ua.org.oa.neplokhv.cinema.dao.api.Dao;
import ua.org.oa.neplokhv.cinema.dto.UserDto;
import ua.org.oa.neplokhv.cinema.mapper.BeanMapper;
import ua.org.oa.neplokhv.cinema.model.User;
import ua.org.oa.neplokhv.cinema.service.api.Service;

import java.util.List;

/**
 * Created by student on 13.09.2016.
 */
public class UserServiceImpl implements Service<Integer, UserDto> {

    private static UserServiceImpl service;
    private Dao<Integer, User> userDao;
    private BeanMapper beanMaper;

    private UserServiceImpl() {
        userDao = DaoFactory.getInstance().getUserDao();
        beanMaper = BeanMapper.getInstance();
    }

    public static synchronized UserServiceImpl getInstance() {
        if (service == null) {
            service = new UserServiceImpl();
        }
        return service;
    }

    @Override
    public List<UserDto> getAll() {
        List<User> users = userDao.getAll();
        List<UserDto> usersDtos = beanMaper.mapAll(users, UserDto.class);
        return usersDtos;
    }

    @Override
    public List<UserDto> getAllOrderedBy(String colName) {
        return null;
    }

    @Override
    public List<UserDto> getByFKey(Integer key, String table) {
        return null;
    }

    @Override
    public List<UserDto> getByStringColumnValue(String columnName, String value) {
        List<User> users = userDao.getByStringColumnValue(columnName, value);
        List<UserDto> usersDtos = beanMaper.mapAll(users, UserDto.class);
        return usersDtos;
    }

    @Override
    public UserDto getById(Integer key) {
        UserDto userDto = beanMaper.singleMapper(userDao.getById(key), UserDto.class);
        return userDto;
    }

    @Override
    public void save(UserDto elem) {
        User user = beanMaper.singleMapper(elem, User.class);
        userDao.save(user);
    }

    @Override
    public void delete(Integer key) {
        userDao.delete(key);
    }

    @Override
    public void update(UserDto elem) {
        User user = beanMaper.singleMapper(elem, User.class);
        userDao.update(user);
    }
}