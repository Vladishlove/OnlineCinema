package ua.org.oa.neplokhv.cinema.controllers;

import ua.org.oa.neplokhv.cinema.service.impl.SeanceServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Vlad on 10/9/2016.
 */
public class DeleteSeanceServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SeanceServiceImpl seanceService = SeanceServiceImpl.getInstance();
        seanceService.delete(Integer.parseInt(request.getParameter("id")));
        response.sendRedirect("\\pages\\admin\\movies.jsp");
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }
}
