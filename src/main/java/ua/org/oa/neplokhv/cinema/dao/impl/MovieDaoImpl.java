package ua.org.oa.neplokhv.cinema.dao.impl;

import ua.org.oa.neplokhv.cinema.model.Movie;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by tssupport15 on 13.09.16.
 */
public class MovieDaoImpl extends CrudDao<Movie> {

    private final String INSERT = "INSERT INTO " +
            "cinema.movie(movieName, description, duration, startDate, endDate) " +
            "VALUES (?,?,?,?,?)";
    private final String UPDATE = "UPDATE cinema.movie " +
            "SET movieName = ?, description = ?, duration = ?, startDate = ?, endDate = ?" +
            "WHERE idmovie = ?";



    private static MovieDaoImpl crudDao;
    public MovieDaoImpl(Class type) {
        super(type);
    }

    public static synchronized MovieDaoImpl getInstance() {
        if (crudDao == null) {
            crudDao = new MovieDaoImpl(Movie.class);
        }
        return crudDao;
    }

    @Override
    public PreparedStatement createInsertPreparedStatement(Connection connection, Movie elem) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, elem.getMovieName());
        preparedStatement.setString(2, elem.getDescription());
        preparedStatement.setInt(3, elem.getDuration());

        java.util.Date start = Date.from(elem.getStartDate().toInstant(ZoneOffset.UTC));
        preparedStatement.setDate(4, new Date(start.getTime()));
        java.util.Date end = Date.from(elem.getEndDate().toInstant(ZoneOffset.UTC));
        preparedStatement.setDate(5, new Date(end.getTime()));
        return preparedStatement;
    }
    @Override
    public PreparedStatement createUpdatePreparedStatement(Connection connection, Movie elem) throws SQLException{
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        preparedStatement.setString(1, elem.getMovieName());
        preparedStatement.setString(2, elem.getDescription());
        preparedStatement.setInt(3, elem.getDuration());
        java.util.Date start = Date.from(elem.getStartDate().toInstant(ZoneOffset.UTC));
        preparedStatement.setDate(4, new Date(start.getTime()));
        java.util.Date end = Date.from(elem.getEndDate().toInstant(ZoneOffset.UTC));
        preparedStatement.setDate(5, new Date(end.getTime()));
        preparedStatement.setInt(6, elem.getId());
        return preparedStatement;
    }
    @Override
    public List<Movie> readAll(ResultSet rs) throws SQLException {
        List<Movie> movies = new ArrayList<>();
        Movie movie = null;
        while (rs.next()) {
            movie = new Movie();
            movie.setId(rs.getInt("idmovie"));
            movie.setMovieName(rs.getString("movieName"));
            movie.setDescription(rs.getString("description"));
            movie.setDuration(rs.getInt("duration"));
            LocalDate start = rs.getDate("startDate").toLocalDate();
            movie.setStartDate(LocalDateTime.of(start, LocalTime.MIN));
            LocalDate end = rs.getDate("endDate").toLocalDate();
            movie.setEndDate(LocalDateTime.of(end, LocalTime.MIN));
            movies.add(movie);
        }
        return movies;
    }
}