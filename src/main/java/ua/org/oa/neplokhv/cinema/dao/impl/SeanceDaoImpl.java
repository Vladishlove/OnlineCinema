package ua.org.oa.neplokhv.cinema.dao.impl;

import ua.org.oa.neplokhv.cinema.model.Seance;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vlad on 9/18/2016.
 */
public class SeanceDaoImpl extends CrudDao<Seance>{
    private final String INSERT = "INSERT INTO " +
            "cinema.seance(dateTime, Hall_idHall, movie_idMovie, ticketPrice) " +
            "VALUES (?,?,?,?)";
    private final String UPDATE = "UPDATE cinema.seance " +
            "SET dateTime = ?, Hall_idHall = ?, movie_idMovie = ?, ticketPrice = ? " +
            "WHERE idseance = ?";

    private static SeanceDaoImpl crudDao;
    private static HallDaoImpl hallDao = HallDaoImpl.getInstance();
    private static MovieDaoImpl movieDao = MovieDaoImpl.getInstance();
    private static TicketDaoImpl ticketDao = TicketDaoImpl.getInstance();

    public SeanceDaoImpl(Class type) {
        super(type);
    }

    public static synchronized SeanceDaoImpl getInstance() {
        if (crudDao == null) {
            crudDao = new SeanceDaoImpl(Seance.class);
        }
        return crudDao;
    }

    @Override
    public PreparedStatement createInsertPreparedStatement(Connection connection, Seance elem) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setTimestamp(1, Timestamp.valueOf(elem.getDateTime()));
        preparedStatement.setInt(2, elem.getHall().getId());
        preparedStatement.setInt(3, elem.getMovie().getId());
        preparedStatement.setInt(4, elem.getTicketPrice());
        return preparedStatement;
    }
    @Override
    public PreparedStatement createUpdatePreparedStatement(Connection connection, Seance elem) throws SQLException{
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        preparedStatement.setTimestamp(1, Timestamp.valueOf(elem.getDateTime()));
        preparedStatement.setInt(2, elem.getHall().getId());
        preparedStatement.setInt(3, elem.getMovie().getId());
        preparedStatement.setInt(4, elem.getTicketPrice());
        preparedStatement.setInt(5, elem.getId());
        return preparedStatement;
    }
    @Override
    public List<Seance> readAll(ResultSet rs) throws SQLException {
        List<Seance> seances = new ArrayList<>();
        Seance seance;
//        System.out.println("in seance readall");
        while (rs.next()) {
            seance = new Seance();
            seance.setId(rs.getInt("idseance"));
            seance.setDateTime(rs.getTimestamp("dateTime").toLocalDateTime());
            seance.setSoldTickets(ticketDao.getByForeignKey(seance.getId(), "seance"));
            seance.setHall(hallDao.getById(rs.getInt("hall_idHall")));
            seance.setMovie(movieDao.getById(rs.getInt("movie_idMovie")));
            seance.setTicketPrice(rs.getInt("ticketPrice"));
            seances.add(seance);
        }
        return seances;
    }
}