package ua.org.oa.neplokhv.cinema.dto;

import ua.org.oa.neplokhv.cinema.model.Entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vlad on 12.08.16.
 */
public class SeanceDto extends Entity implements Serializable {
    private LocalDateTime dateTime;
    private HallDto hall;
    private MovieDto movie;
    private int ticketPrice;
    private List<TicketDto> soldTickets;

    public int getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(int ticketPrice) {
        this.ticketPrice = ticketPrice;
    }


    private boolean[][] placesMap;

    public SeanceDto(){}

    public SeanceDto(LocalDateTime d, HallDto h, MovieDto m) {
        this.dateTime = d;
        this.hall = h;
        this.movie = m;
        placesMap = new boolean[hall.getRowQuantity()][hall.getPlaceQuantity()];
    }

    public void setDateTime(LocalDateTime date) {
        this.dateTime = date;
    }
    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public HallDto getHall() {
        return hall;
    }

    public void setHall(HallDto h) {
        this.hall = h;
        if (placesMap == null) {
            placesMap = new boolean[hall.getRowQuantity()][hall.getPlaceQuantity()];
        }
        if (soldTickets == null) {
            soldTickets = new ArrayList<>();
        }
        for (TicketDto soldTicket : soldTickets) {
            setOccupied(soldTicket.getRow(),soldTicket.getPlace());
        }
    }

    public MovieDto getMovie() {
        return movie;
    }

    public void setMovie(MovieDto movie) {
        this.movie = movie;
    }
    public boolean[][] getPlacesMap() {
        return placesMap;
    }
    public void viewPlacesMap(){
        for (int i = 0; i < hall.getRowQuantity(); i++){
            for (int j = 0; j < hall.getPlaceQuantity(); j++) {
                System.out.print(placesMap[j][i] + " ");
            }
            System.out.println();
        }
    }
    public boolean isOccupied(int r, int p) {
        return placesMap[r-1][p-1];
    }
    public void setOccupied(int row, int place) {
        placesMap[row-1][place-1] = true;  //now this place is occupied
    }

    @Override
    public String toString() {
        return "Seance{ id = " + getId() +
                ", date=" + getDateTime() +
                ", hall=" + hall +
                ", movie=" + movie +
                '}'+"\n\r";
    }

    public List<TicketDto> getSoldTickets() {
        return soldTickets;
    }

    public void setSoldTickets(List<TicketDto> soldTickets) {
        this.soldTickets = soldTickets;
    }
}