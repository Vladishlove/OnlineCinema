package ua.org.oa.neplokhv.cinema.listeners;

import ua.org.oa.neplokhv.cinema.dto.*;
import ua.org.oa.neplokhv.cinema.helpers.PropertyHolder;
import ua.org.oa.neplokhv.cinema.model.Seance;
import ua.org.oa.neplokhv.cinema.service.impl.*;
import ua.org.oa.neplokhv.cinema.web.Manager;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import static ua.org.oa.neplokhv.cinema.helpers.FileHelper.pathSeparator;

/**
 * Created by Vlad on 10/3/2016.
 */
public class ContextListener implements ServletContextListener{
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        List<SeanceDto> seanceDtos = SeanceServiceImpl.getInstance().getAll();
        List<MovieDto> movieDtos = MovieServiceImpl.getInstance().getAll();
        ServletContext ctx = servletContextEvent.getServletContext();
        ctx.setAttribute("seances", seanceDtos);
        ctx.setAttribute("movies", movieDtos);
        Properties properties = new Properties();
        UserDto userDto = null;
        boolean exists = false;
        List<UserDto> userDtos = UserServiceImpl.getInstance().getAll();
        for (UserDto userDto1 : userDtos) {
            if (userDto1.getRole().getName().equals("admin")) {
                exists = true;
            }
        }
        if (!exists) {
            try {
                System.out.println("In ctx lstnr try block");
                properties.load(PropertyHolder.class.getClassLoader().getResourceAsStream("cinema" + pathSeparator + "db.properties"));
                String userLogin = properties.getProperty("cinemaAdminLogin");
                String userPass = properties.getProperty("cinemaAdminPassword");
                String userRole = properties.getProperty("cinemaAdminRole");
                userDto = Manager.fillUserData(userLogin, userPass, userRole);
                UserServiceImpl.getInstance().save(userDto);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        exists = false;
        for (UserDto userDto1 : userDtos) {
            if (userDto1.getRole().getName().equals("user")) {
                exists = true;
            }
        }
        if (!exists) {
            try {
                properties.load(PropertyHolder.class.getClassLoader().getResourceAsStream("cinema" + pathSeparator + "db.properties"));
                String userLogin = properties.getProperty("cinemaUserLogin");
                String userPass = properties.getProperty("cinemaUserPassword");
                String userRole = properties.getProperty("cinemaUserRole");
                userDto = Manager.fillUserData(userLogin, userPass, userRole);
                UserServiceImpl.getInstance().save(userDto);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
