package ua.org.oa.neplokhv.cinema.helpers;

import java.io.IOException;
import java.util.Properties;
import static ua.org.oa.neplokhv.cinema.helpers.FileHelper.pathSeparator;


/**
 * Created by Vlad on 9/12/2016.
 */
public class PropertyHolder {

    private boolean inMemory;
    private String URL;
    private String userLogin;
    private String password;
    private String dbDriver;

    private static PropertyHolder propertyHolder;
    private PropertyHolder() {
        loadProperties();
    }

    public static PropertyHolder getInstance() {
        if (propertyHolder == null) {
            propertyHolder = new PropertyHolder();
        }
        return propertyHolder;
    }

    private void loadProperties() {
        Properties properties = new Properties();
        try{
            properties.load(PropertyHolder.class.getClassLoader().getResourceAsStream("cinema"+pathSeparator+"db.properties"));
            inMemory = Boolean.valueOf(properties.getProperty("db.inMemory"));
            URL = properties.getProperty("jdbcURL");
            userLogin = properties.getProperty("dbUserLogin");
            password = properties.getProperty("dbUserPassword");
            dbDriver = properties.getProperty("dbDriver");
        }catch (IOException e) {
            e.printStackTrace();
        }

    }

    public boolean isInMemory() {
        return inMemory;
    }

    public String getURL() {
        return URL;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public String getPassword() {
        return password;
    }

    public String getDbDriver() {
        return dbDriver;
    }
}