package ua.org.oa.neplokhv.cinema.model;

/**
 * Created by vlad on 12.08.16.
 */
public abstract class Entity {
    int id;

    public Entity(int id) {
        setId(id);
    }
    public Entity() {

    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

}
