package ua.org.oa.neplokhv.cinema.helpers;

import java.time.LocalDateTime;

/**
 * Created by Vlad on 10/14/2016.
 */
public class DateNavigation {

    public static LocalDateTime setSelectedDate(LocalDateTime sessionAttr, String requestParameter) {
        if (sessionAttr == null
                && requestParameter == null){
            return LocalDateTime.now();
        }
        else {
            if (requestParameter != null) {
                return LocalDateTime.parse(requestParameter);
            }
            else {
                return sessionAttr;
            }
        }
    }
}
