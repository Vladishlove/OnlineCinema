package ua.org.oa.neplokhv.cinema.model;

/**
 * Created by vlad on 12.08.16.
 */
public class InMemoryDB {
    public static Storage<User> userStorage = new Storage<>();
    public static Storage<Seance> seansStorage = new Storage<>();
    public static Storage<Hall> hallStorage = new Storage<>();
    public static Storage<Ticket> ticketStorage = new Storage<>();
    public static Storage<Movie> movieStorage = new Storage<>();
}
