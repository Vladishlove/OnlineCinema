package ua.org.oa.neplokhv.cinema.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vlad on 12.08.16.
 */
public class Seance extends Entity implements Serializable {
    private LocalDateTime dateTime;
    private Hall hall;
    private Movie movie;
    private int ticketPrice;
    private List<Ticket> soldTickets;
    private boolean[][] placesMap;

    public Seance(){}

    public Seance(LocalDateTime d, Hall h, Movie m) {
        this.dateTime = d;
        this.hall = h;
        this.movie = m;
        placesMap = new boolean[hall.getRowQuantity()][hall.getPlaceQuantity()];
    }
    public int getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(int ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public void setDateTime(LocalDateTime date) {
        this.dateTime = date;
    }
    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall h) {
        this.hall = h;
        if (placesMap == null) {
            placesMap = new boolean[hall.getRowQuantity()][hall.getPlaceQuantity()];
        }
        if (soldTickets == null) {
            soldTickets = new ArrayList<>();
        }
        for (Ticket soldTicket : soldTickets) {
            setOccupied(soldTicket.getRow(),soldTicket.getPlace());
        }
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }
    public boolean[][] getPlacesMap() {
        return this.placesMap;
    }
    public void viewPlacesMap(){
        for (int i = 0; i < hall.getRowQuantity(); i++){
            for (int j = 0; j < hall.getPlaceQuantity(); j++) {
                System.out.print(placesMap[j][i] + " ");
            }
            System.out.println();
        }
    }
    public boolean isOccupied(int r, int p) {
        return placesMap[r][p];
    }
    public void setOccupied(int row, int place) {
        placesMap[row-1][place-1] = true;  //now this place is occupied
    }

    @Override
    public String toString() {
        return "Seance{id = " + getId() +
                ", date=" + getDateTime() +
                ", hall=" + hall +
                ", movie=" + movie +
                '}'+"\n\r";
    }

    public List<Ticket> getSoldTickets() {
        return soldTickets;
    }

    public void setSoldTickets(List<Ticket> soldTickets) {
        this.soldTickets = soldTickets;
    }
}