package ua.org.oa.neplokhv.cinema.dto;

import ua.org.oa.neplokhv.cinema.model.Entity;

/**
 * Created by tssupport15 on 16.09.16.
 */
public class TicketDto extends Entity {public TicketDto() {
}
    private int place;
    private int row;
    private int seanceId;
    private int userId;
    private boolean isSold;


    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }
    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getSeanceId() {
        return seanceId;
    }

    public void setSeanceId(int seance) {
        this.seanceId = seance;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isSold() {
        return isSold;
    }

    public void setSold(boolean sold) {
        isSold = sold;
    }

    @Override
    public String toString() {
        return "Ticket{" +"id=" + getId() +
                ", place=" + place +
                ", row=" + row +
                ", seanceId=" + seanceId + ", userId=" + getUserId() +
                '}';
    }
}