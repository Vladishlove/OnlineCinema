package ua.org.oa.neplokhv.cinema.dao.impl;

import ua.org.oa.neplokhv.cinema.model.Hall;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tssupport15 on 16.09.16.
 */
public class HallDaoImpl extends CrudDao<Hall>{
    private final String INSERT = "INSERT INTO " +
        "cinema.hall(rowQuantity, placeQuantity, name) " +
        "VALUES (?,?,?)";
    private final String UPDATE = "UPDATE cinema.hall " +
            "SET rowQuantity = ?, placeQuantity = ?, name = ? " +
            "WHERE idHall = ?";

    private static HallDaoImpl hallDao;
    public HallDaoImpl(Class type) {
        super(type);
    }

    public static synchronized HallDaoImpl getInstance() {
        if (hallDao == null) {
            hallDao = new HallDaoImpl(Hall.class);
        }
        return hallDao;
    }

    @Override
    public PreparedStatement createInsertPreparedStatement(Connection connection, Hall elem)
            throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setInt(1, elem.getRowQuantity());
        preparedStatement.setInt(2, elem.getPlaceQuantity());
        preparedStatement.setString(3, elem.getName());
        return preparedStatement;
    }
    @Override
    public PreparedStatement createUpdatePreparedStatement(Connection connection, Hall elem)
            throws SQLException{
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
        preparedStatement.setInt(1, elem.getRowQuantity());
        preparedStatement.setInt(2, elem.getPlaceQuantity());
        preparedStatement.setString(3, elem.getName());
        preparedStatement.setInt(4, elem.getId());
        return preparedStatement;
    }
    @Override
    public List<Hall> readAll(ResultSet rs) throws SQLException {
        List<Hall> halls = new ArrayList<>();
        Hall hall;
        while (rs.next()) {
            hall = new Hall();
            hall.setId(rs.getInt("idHall"));
            hall.setRowQuantity(rs.getInt("rowQuantity"));
            hall.setPlaceQuantity(rs.getInt("placeQuantity"));
            hall.setName(rs.getString("name"));
            halls.add(hall);
        }
        return halls;
    }
}