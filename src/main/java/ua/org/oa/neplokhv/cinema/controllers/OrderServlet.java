package ua.org.oa.neplokhv.cinema.controllers;

import ua.org.oa.neplokhv.cinema.dto.SeanceDto;
import ua.org.oa.neplokhv.cinema.dto.UserDto;
import ua.org.oa.neplokhv.cinema.model.Seance;
import ua.org.oa.neplokhv.cinema.service.impl.SeanceServiceImpl;
import ua.org.oa.neplokhv.cinema.service.impl.UserServiceImpl;
import ua.org.oa.neplokhv.cinema.web.Manager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static ua.org.oa.neplokhv.cinema.helpers.FileHelper.pathSeparator;

/**
 * Created by Vlad on 10/5/2016.
 */
public class OrderServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int seanceId = (int) request.getSession().getAttribute("seanceId");
        SeanceDto seance = SeanceServiceImpl.getInstance().getById(seanceId);
        UserDto user = (UserDto) request.getSession().getAttribute("user");
        String[] checked = request.getParameterValues("rowPlace");
        if (checked == null) {
            response.sendRedirect(getServletContext().getContextPath()+pathSeparator+"seance?id="+seanceId);
        }
        else if (user == null) {
            request.getSession().setAttribute("seance", seance);
            request.getSession().setAttribute("urlToReturn",
                    getServletContext().getContextPath() + pathSeparator
                    +"pages"+pathSeparator +
                    "common" + pathSeparator + "seance.jsp");
            response.sendRedirect(getServletContext().getContextPath() + "/pages/errors/roleUserErrorPage.jsp");
        }
        else {
            for (String s : checked) {
                int row = Integer.parseInt(s.split(":")[0]);
                int place = Integer.parseInt(s.split(":")[1]);
                Manager.buyTicket(row, place, seance, user);
            }
            seance = SeanceServiceImpl.getInstance().
                    getById((seanceId));
            request.getSession().setAttribute("seance", seance);
            int ticketsAmount = checked.length;
            request.getSession().setAttribute("ticketsAmountInOrder", ticketsAmount);
            //Updating user in session(ticketList)
            request.getSession().setAttribute("user", UserServiceImpl.getInstance().getById(user.getId()));
            response.sendRedirect(getServletContext().getContextPath() + pathSeparator
                    +"pages"+pathSeparator+
                    user.getRole().getName() + pathSeparator + "order.jsp");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }
}