package ua.org.oa.neplokhv.cinema.controllers;

import ua.org.oa.neplokhv.cinema.dto.UserDto;
import ua.org.oa.neplokhv.cinema.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import static ua.org.oa.neplokhv.cinema.helpers.FileHelper.pathSeparator;

/**
 * Created by Vlad on 9/27/2016.
 */
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserServiceImpl userService;
        PrintWriter writer = response.getWriter();
        UserDto user;
        HttpSession session = request.getSession();
        user = (UserDto) session.getAttribute("user");
        if (user != null) {
            response.sendRedirect(getServletContext().getContextPath() + pathSeparator +
                    "pages" + pathSeparator + "user" + pathSeparator +
                    "movies.jsp");
        } else if (user == null) {
            String login = request.getParameter("login");
            String password = request.getParameter("password");
            userService = UserServiceImpl.getInstance();
            List<UserDto> result = userService.getByStringColumnValue("login", login);
            if (!result.isEmpty()) {
                user = userService.getByStringColumnValue("login", login).get(0);
                if (user.getPassword().equals(password)) {
                    session.setAttribute("user", user);
                    if (request.getSession().getAttribute("urlToReturn") == null) {
                        response.sendRedirect(getServletContext().
                                getContextPath() + pathSeparator + "pages" + pathSeparator + user.getRole().getName() + pathSeparator + "movies.jsp");
                    } else {
                        String urlToReturnStr = (String) request.getSession().getAttribute("urlToReturn");
                        response.sendRedirect(urlToReturnStr.replace("common", user.getRole().getName()));
                    }
                } else {
                    response.sendRedirect(request.getContextPath() + pathSeparator +
                            "pages" + pathSeparator + "errors" + pathSeparator + "loginError.jsp");
                }
            } else {
                response.sendRedirect(request.getContextPath() + pathSeparator +
                        "pages" + pathSeparator + "errors" + pathSeparator + "loginError.jsp");
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserDto user = (UserDto) request.getSession().getAttribute("user");
        if (user != null) {
            response.sendRedirect(getServletContext().getContextPath() + pathSeparator +
                    "pages" + pathSeparator + user.getRole().getName() + pathSeparator + "movies.jsp");
        } else {
            response.sendRedirect(getServletContext().getContextPath() + pathSeparator +
                    "pages" + pathSeparator + "common" + pathSeparator + "login.jsp");
        }
    }
}