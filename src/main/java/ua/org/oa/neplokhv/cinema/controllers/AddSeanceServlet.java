package ua.org.oa.neplokhv.cinema.controllers;


import ua.org.oa.neplokhv.cinema.dto.HallDto;
import ua.org.oa.neplokhv.cinema.dto.MovieDto;
import ua.org.oa.neplokhv.cinema.model.Hall;
import ua.org.oa.neplokhv.cinema.model.Seance;
import ua.org.oa.neplokhv.cinema.service.impl.HallServiceImpl;
import ua.org.oa.neplokhv.cinema.service.impl.MovieServiceImpl;
import ua.org.oa.neplokhv.cinema.web.Manager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import static ua.org.oa.neplokhv.cinema.helpers.FileHelper.pathSeparator;

/**
 * Created by Vlad on 9/27/2016.
 */
public class AddSeanceServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        String htm =
                "<body>" +
                        "<p>" + "Seance is saved" +
                        "<p>" + "Time: " + request.getParameter("dateTime").replace("T", " ") + "<br/>" +
                        "Hall id: " + request.getParameter("halls") + "<br/>" +
                        "Movie id: " + request.getParameter("movies") + "<br/>" +
                        "ticketPrice: " + request.getParameter("ticketPrice") + "<br />";


        try {
            Manager.createSeance(
                    MovieServiceImpl.getInstance().getById(Integer.parseInt(request.getParameter("movies"))),
                    HallServiceImpl.getInstance().getById(Integer.parseInt(request.getParameter("halls"))),
                    LocalDateTime.parse(request.getParameter("dateTime")),
                    Integer.parseInt(request.getParameter("ticketPrice")));
            request.getSession().setAttribute("seanceMessage", htm);
            response.sendRedirect(getServletContext().getContextPath() +
                    pathSeparator + "pages" + pathSeparator
                    + "admin" + pathSeparator + "addSeance.jsp");
        } catch (IllegalArgumentException e) {
            request.getSession().setAttribute("seanceMessage", e.getMessage());
            response.sendRedirect(getServletContext().getContextPath() +
                    pathSeparator + "pages" + pathSeparator
                    + "admin" + pathSeparator + "addSeance.jsp");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<MovieDto> movieDtos = MovieServiceImpl.getInstance().getAll();
        List<HallDto> hallDtos = HallServiceImpl.getInstance().getAll();
        request.getSession().setAttribute("movies", movieDtos);
        request.getSession().setAttribute("halls", hallDtos);
        response.sendRedirect(getServletContext().getContextPath()+pathSeparator+"pages" + pathSeparator
                + "admin" + pathSeparator + "addSeance.jsp");
    }
}