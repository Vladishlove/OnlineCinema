package ua.org.oa.neplokhv.cinema.model;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by vlad on 12.08.16.
 */
public class User extends Entity implements Serializable {

    private String name;
    private String lastName;
    private String login;
    private String password;
    private String email;
    private List<Ticket> ticketList;
    private Role role;

    public User(){
        ticketList = new ArrayList<>();
    }
    public User(String name, String lastName, String login, String password, String email){
        setName(name);
        setLastName(lastName);
        setLogin(login);
        setPassword(password);
        setEmail(email);
        ticketList = new ArrayList<>();
        role = new Role("User");
    }
    public User(String name, String login, String password, Role r){
        setName(name);
        setLogin(login);
        setPassword(password);
        ticketList = new ArrayList<>();
        role = r;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void addTicket(Ticket t) {
        ticketList.add(t);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Ticket> getTicketList() {
        return ticketList;
    }

    public void setTicketList(List<Ticket> ticketList) {
        this.ticketList = ticketList;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", ticketList=" + ticketList +
                ", role=" + role +
                '}';
    }
}